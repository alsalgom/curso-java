/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaframe;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Iss
 */
public class Biblioteca {

    private Set<Socio> socios;
    private Set<Obra> obras;
    private final int MAXSOCIOS = 50;
    private final int MAXOBRAS = 50;
    private int obrasActuales;
    private int sociosActuales;
    private IGestorBiblioteca gestor;
    
    private final String[] GENEROPELI = {"Ficcion", "Misterio", "Thriller", "Accion", "Infantil", "Romantica", "Autobiografica", "Historica"};
    private final String[] GENERONOVELA = {"Ficcion", "Misterio", "Thriller", "Accion", "Infantil", "Romantico", "autobiografico", "Historico"};
    private final String[] GENEROTECNICO = {"Tecnico", "Matematicas", "Quimica", "Fisica", "Arquitectura", "Java", "Programacion", "Calculo"};
    private final String[] GENEROALBUM = {"Pop","Rock","Indie","Regea"};

    public Biblioteca(IGestorBiblioteca gestor) {
        this.socios = new HashSet<Socio>();
        this.obras = new HashSet<Obra>();
        this.obrasActuales = 0;
        this.sociosActuales = 0;
        this.gestor = gestor;
        
    }

    public String[] getGENEROALBUM() {
        return GENEROALBUM;
    }

    public int getMAXSOCIOS() {
        return MAXSOCIOS;
    }

    public int getMAXOBRAS() {
        return MAXOBRAS;
    }

    public String[] getGENEROPELI() {
        return GENEROPELI;
    }

    public String[] getGENERONOVELA() {
        return GENERONOVELA;
    }

    public String[] getGENEROTECNICO() {
        return GENEROTECNICO;
    }

    public Set<Socio> getSocios() {
        return socios;
    }

    public void setSocios(Set<Socio> socios) {
        this.socios = socios;
    }

    public Set<Obra> getObras() {
        return obras;
    }

    public void setObras(Set<Obra> obras) {
        this.obras = obras;
    }

   

    public int getObrasActuales() {
        return obrasActuales;
    }

    public void setObrasActuales(int obrasActuales) {
        this.obrasActuales = obrasActuales;
    }

    public int getSociosActuales() {
        return sociosActuales;
    }

    public void setSociosActuales(int sociosActuales) {
        this.sociosActuales = sociosActuales;
    }

    public IGestorBiblioteca getGestor() {
        return gestor;
    }

    public void setGestor(IGestorBiblioteca gestor) {
        this.gestor = gestor;
    }

    public void cargarObras(String direccion) {
        this.obras = this.gestor.cargarObras(direccion);
    }

    public void guardarObras(String direccion) {
        this.gestor.guardarObras(direccion, this.getObras());
    }

    public void cargarSocios(String direccion) {
        this.socios = this.gestor.cargarSocios(direccion);
    }

    public void guardarSocios(String direccion) {
        this.gestor.guardarSocio(direccion, this.getSocios());
    }

    public boolean insertarSocio(Persona p) {
        if (p instanceof Socio && this.getSociosActuales() < this.MAXSOCIOS) {
            this.setSociosActuales(this.getSociosActuales() + 1);
            return this.socios.add((Socio) p);
        } 
        return false;
    }

    public boolean insertarObra(Obra o) {
        if (o instanceof Obra && this.getObrasActuales() < this.MAXOBRAS) {
            this.setObrasActuales(this.getObrasActuales() + 1);
            return this.obras.add(o);
        } 
        return false;
    }

    public String imprimirSocios() {
        StringBuilder str = new StringBuilder();
        this.socios.stream().forEach((s) -> {
            str.append(s.toString());
            str.append("\n");
        });
        return str.toString();
    }

    public String imprimirObras() {
        StringBuilder str = new StringBuilder();
        this.obras.stream().forEach((o) -> {
            str.append(o.toString());
            str.append("\n");
        });
        return str.toString();
    }

    public HashSet<Obra> noDevueltos() {
        HashSet<Obra> s = new HashSet<>();

        this.obras.stream().forEach((o) -> {
            if (o != null) {
                if (o.isEnPrestamo()) {
                    s.add(o);
                }
            }
        });
        return s;
    }

    public boolean prestarObra(Socio socio, Obra obra) {

        return false;
    }

    public boolean retornarObra(Socio socio, Obra obra) {
        return false;
    }

    @Override
    public String toString() {
        String s = "";
        s = "OBRAS EN LA BIBLIOTECA";
        s = s + this.imprimirObras();
        s = "SOCIOS EN LA BIBLIOTECA";
        s = s + this.imprimirSocios();
        return s;
    }
}
