/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaframe;

import java.util.HashSet;

/**
 *
 * @author Iss
 */
public class Controlador {

    private Biblioteca biblioteca;
    private IGestorBiblioteca gestor;
    private Ventana v;

    public Controlador() {

        gestor = new GestorBibliotecaSerializable();
        this.biblioteca = new Biblioteca(gestor);
        this.v = new Ventana(this);
        v.setVisible(true);

    }

    public Biblioteca getBiblioteca() {
        return biblioteca;
    }

    public void setBiblioteca(Biblioteca biblioteca) {
        this.biblioteca = biblioteca;
    }

    public void obrasNoDevueltas() {
        int i = 0, j = 0;
        HashSet<Obra> ob = biblioteca.noDevueltos();
        ob.forEach((o) -> {
            System.out.println(o.toString());
        });
    }

    public Socio existeSocio(int socio) {
        for (Persona p : biblioteca.getSocios()) {
            if (p != null) {
                if (((Socio) p).getNumSocio() == socio) {
                    System.out.println("Socio encontrado");
                    return (Socio) p;
                }
            }
        }
        return null;
    }

    public Obra existeObra(String obra) {
        for (Obra o : biblioteca.getObras()) {
            if (o != null) {
                if (o.getIsbn().equals(obra)) {
                    System.out.println("obra encontrada");
                    return o;
                }
            }
        }
        return null;
    }

    public boolean insertarObra(Obra o) {
        return this.biblioteca.insertarObra(o);
    }

    public boolean insertaSocio(Socio s) {
        return this.biblioteca.insertarSocio(s);
    }

    public boolean retornarObra(String obra, int socio) {
        Socio s = this.existeSocio(socio);
        Obra o = this.existeObra(obra);
        if (s != null && o != null) {
            return biblioteca.retornarObra(s, o);
        } else {
            if (s == null) {
                System.out.println("Socio no exite");
            }
            if (o == null) {
                System.out.println("Obra no existe");
            }
        }

        return false;
    }

    public boolean prestarObra(String obra, int socio) {
        Socio s = this.existeSocio(socio);
        Obra o = this.existeObra(obra);
        if (s != null && o != null) {
            return biblioteca.retornarObra(s, o);
        } else {
            if (s == null) {
                System.out.println("Socio no exite");
            }
            if (o == null) {
                System.out.println("Obra no existe");
            }
        }

        return false;
    }

    public void cargarObras(String direccion) {
        this.biblioteca.cargarObras(direccion);
    }

    public void guardarObras(String direccion) {
        this.biblioteca.guardarObras(direccion);
    }

    public void cargarSocios(String direccion) {
        this.biblioteca.cargarSocios(direccion);
    }

    public void guardarSocios(String direccion) {
        this.biblioteca.guardarSocios(direccion);
    }

    @Override
    public String toString() {
        return biblioteca.toString();
    }

}
