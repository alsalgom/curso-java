/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaframe;

import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author Iss
 */
public interface IGestorBiblioteca {

    public Set< Obra> cargarObras(String Fichero);

    public Set<Socio> cargarSocios(String Fichero);

    public void guardarObras(String Fichero, Set< Obra> obras);

    public void guardarSocio(String Fichero, Set<Socio> socios);
}
