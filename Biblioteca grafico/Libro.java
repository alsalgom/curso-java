/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaframe;

/**
 *
 * @author Iss
 */
public class Libro extends Obra {

    public String tipo;
    public String autor;

    public Libro(int MAXPRESTAMO) {
        super(MAXPRESTAMO);
    }

    public Libro(String isbn, String titulo, String genero, int MAXPRESTAMO) {
        super(isbn, titulo, genero, MAXPRESTAMO);
    }

    public Libro(String isbn,String titulo,String autor,String tipo, String genero, int MAXPRESTAMO) {
        super(isbn, titulo, genero, MAXPRESTAMO);
        this.tipo = tipo;
        this.autor = autor;

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    @Override
    public String toString() {
        return super.toString() + ", Autor: " + this.getAutor() + ", Genero: " + this.getGenero() + ", Tipo: " + this.getTipo() + "";
    }
}
