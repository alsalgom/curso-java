/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaframe;

/**
 *
 * @author Iss
 */
public class LibroTecnico extends Libro {

    public LibroTecnico() {
        super(7);
    }

    public LibroTecnico(String isbn, String titulo, String genero) {
        super(isbn, titulo, genero, 7);

    }

    public LibroTecnico(String isbn, String autor, String titulo, String genero) {
        super(isbn, titulo, autor, "Libro Tecnico", genero, 7);

    }

    @Override
    public String toString() {
        return super.toString() + " Clase: " + this.getClass();
    }

}
