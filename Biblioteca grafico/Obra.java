/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaframe;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author Iss
 */
public abstract class Obra implements Serializable {

    private String titulo, isbn;
    private Hashtable<Integer, Socio> historialPrestamo;
    private final int MAXPRESTAMO;
    private boolean enPrestamo;
    private Date fechaPrestamo;
    private String genero;

    public Obra(int MAXPRESTAMO) {
        this.isbn = "";
        this.titulo = "";
        this.genero = "";
        this.historialPrestamo = new Hashtable<Integer, Socio>();
        this.MAXPRESTAMO = MAXPRESTAMO;
        this.enPrestamo = false;
        this.fechaPrestamo = null;

    }

    public Obra(String isbn, String titulo, String genero, int MAXPRESTAMO) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.genero = genero;
        this.historialPrestamo = new Hashtable<Integer, Socio>();
        this.MAXPRESTAMO = MAXPRESTAMO;
        this.enPrestamo = false;
        this.fechaPrestamo = null;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Hashtable<Integer, Socio> getHistorialPrestamo() {
        return historialPrestamo;
    }

    public void setHistorialPrestamo(Hashtable<Integer, Socio> historialPrestamo) {
        this.historialPrestamo = historialPrestamo;
    }

    public boolean isEnPrestamo() {
        return enPrestamo;
    }

    public void setEnPrestamo(boolean enPrestamo) {
        this.enPrestamo = enPrestamo;
    }

    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public int getMAXPRESTAMO() {
        return MAXPRESTAMO;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "ISBN: " + this.getIsbn() + ", Titulo: " + this.getTitulo() + ", En Prestamo: " + this.isEnPrestamo() + ", Max Prestamo: " + this.MAXPRESTAMO + " dias , Historial: " + this.getHistorialPrestamo().toString();
    }

}
