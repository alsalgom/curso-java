/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliotecaframe;

/**
 *
 * @author Iss
 */
public class Pelicula extends Obra {

    private String director;

    public Pelicula() {
        super(5);
        director = "";

    }

    public Pelicula(String isbn, String titulo, String director, String genero) {
        super(isbn, titulo, genero, 5);
        this.director = director;

    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return super.toString() + ", Director: " + this.getDirector() + " Clase: " + this.getClass();
    }

}
