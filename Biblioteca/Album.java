/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author Iss
 */
public class Album extends Obra {

    public String autor;
    public String[] canciones;

    public Album() {
        super(15);
        this.autor = "";
        this.canciones = new String[0];
    }

    public Album(String autor, String isbn, String titulo, String[] canciones) {
        super(isbn, titulo, 15);
        this.autor = autor;
        this.canciones = canciones;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String[] getCanciones() {
        return canciones;
    }

    public void setCanciones(String[] canciones) {
        this.canciones = canciones;
    }

    @Override
    public String toString() {
        return super.toString() + ", Autor: " + this.getAutor() + ", Canciones: " + Arrays.toString(this.getCanciones()) + " Clase: " + this.getClass();
    }

}
