/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Iss
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Controlador conBiblio = new Controlador();
        int opcion = 0, SALIR = 0, socio = 0;
        String obra = "";
        Scanner entrada = new Scanner(System.in);
        do {
            System.out.println("1) Listar Obras");
            System.out.println("2) Listar Socios");
            System.out.println("3) Listar Obras Prestadas");
            System.out.println("4) Prestar Obra");
            System.out.println("5) Retornar Obra");
            System.out.println("9) Salir");
            opcion = entrada.nextInt();
            switch (opcion) {
                case 1:
                    //listar Obras
                    System.out.println(conBiblio.getBiblioteca().imprimirObras());
                    break;
                case 2:
                    //listar Socios
                    System.out.println(conBiblio.getBiblioteca().imprimirSocios());
                    break;
                case 3:
                    //listar obras prestadas
                    conBiblio.obrasNoDevueltas();
                    break;
                case 4:
                    //prestar obra
                    System.out.println("ID Obra");
                    obra = entrada.next();
                    System.out.println("ID Socio");
                    socio = entrada.nextInt();
                    conBiblio.prestarObra(obra, socio);
                    break;
                case 5:
                    //retornar obra
                    System.out.println("ID Obra");
                    obra = entrada.next();
                    System.out.println("ID Socio");
                    socio = entrada.nextInt();
                    conBiblio.retornarObra(obra, socio);
                    break;
                case 9:
                    //salir
                    SALIR = 1;
                    break;
                default:
                    SALIR = 1;
                    break;
            }

        } while (SALIR == 0);

    }
    

}
