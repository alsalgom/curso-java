/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Iss
 */
public class Biblioteca {

    public Persona[] socios;
    public Obra[] obras;
    public final int MAXSOCIOS = 50;
    public final int MAXOBRAS = 50;
    public int obrasActuales;
    public int sociosActuales;

    public Biblioteca() {
        this.socios = new Persona[MAXSOCIOS];
        this.obras = new Obra[MAXOBRAS];
        this.obrasActuales = 0;
        this.sociosActuales = 0;
    }

    public Persona[] getSocios() {
        return socios;
    }

    public void setSocios(Persona[] socios) {
        this.socios = socios;
    }

    public Obra[] getObras() {
        return obras;
    }

    public void setObras(Obra[] obras) {
        this.obras = obras;
    }

    public int getObrasActuales() {
        return obrasActuales;
    }

    public void setObrasActuales(int obrasActuales) {
        this.obrasActuales = obrasActuales;
    }

    public int getSociosActuales() {
        return sociosActuales;
    }

    public void setSociosActuales(int sociosActuales) {
        this.sociosActuales = sociosActuales;
    }

    public void insertarSocio(Persona p) {
        if (p instanceof Socio && this.getSociosActuales() < this.MAXSOCIOS) {
            this.setSociosActuales(this.getSociosActuales() + 1);
            this.getSocios()[this.getSociosActuales()] = p;
        } else {
            System.out.println("ERROR");
        }
    }

    public void insertarObra(Obra o) {
        if (o instanceof Obra && this.getObrasActuales() < this.MAXOBRAS) {
            this.setObrasActuales(this.getObrasActuales() + 1);
            this.getObras()[this.getObrasActuales()] = o;
        } else {
            System.out.println("ERROR");
        }
    }

    public void insertarSocio(Persona p, int pos) {
        if (p instanceof Socio && this.getSociosActuales() < this.MAXSOCIOS && pos < this.MAXSOCIOS) {
            if (this.getSocios()[pos] == null) {
                this.setSociosActuales(this.getSociosActuales() + 1);
            }
            this.getSocios()[pos] = p;
        } else {
            System.out.println("ERROR");
        }
    }

    public void insertarObra(Obra o, int pos) {
        if (o instanceof Obra && this.getSociosActuales() < this.MAXOBRAS && pos < this.MAXOBRAS) {
            if (this.getObras()[pos] == null) {
                this.setObrasActuales(this.getObrasActuales() + 1);
            }
            this.getObras()[pos] = o;
        } else {
            System.out.println("ERROR");
        }
    }

    public String imprimirSocios() {
        String s = "";
        for (Persona p : this.getSocios()) {
            if (p instanceof Socio) {
                s = s + ((Socio) p).toString() + "\n";
            }
        }
        return s;
    }

    public String imprimirObras() {
        String s = "";
        for (Obra o : this.getObras()) {
            if (o != null) {
                s = s + o.toString() + "\n";
            }
        }
        return s;
    }

    public String[][] noDevueltos() {
        String[][] s = new String[this.getObrasActuales()][this.getObrasActuales()];
        int i = 0, j = 0;
        for (Obra o : this.getObras()) {
            if (o != null) {
                if (o.isEnPrestamo()) {
                    s[i][j] = "Obra: " + o.getTitulo() + " ";
                    s[i][j + 1] = s + o.historialPrestamo.get(o.getFechaPrestamo()).toString() + "\n";
                    i++;
                    j = 0;
                }
            }
        }
        return s;
    }

    public boolean prestarObra(Socio socio, Obra obra) {

        return false;
    }

    public boolean retornarObra(Socio socio, Obra obra) {
        return false;
    }

    @Override
    public String toString() {
        String s = "";
        s = "OBRAS EN LA BIBLIOTECA";
        s = s + this.imprimirObras();
        s = "SOCIOS EN LA BIBLIOTECA";
        s = s + this.imprimirSocios();
        return s;
    }
}
