/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author Iss
 */
public class Libro extends Obra {

    public final String[] TIPOS = {"Novela", "Libro Tecnico"};
    public String tipo;
    public String autor;
    public String genero;

    public Libro(int MAXPRESTAMO) {
        super(MAXPRESTAMO);
    }

    public Libro(int MAXPRESTAMO, String isbn, String titulo) {
        super(isbn, titulo, MAXPRESTAMO);
    }

    public Libro(String tipo, String autor, String genero, int MAXPRESTAMO, String isbn, String titulo) {
        super(isbn, titulo, MAXPRESTAMO);
        this.tipo = tipo;
        this.autor = autor;
        this.genero = genero;

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return super.toString() + ", Autor: " + this.getAutor() + ", Genero: " + this.getGenero() + ", Tipo: " + this.getTipo() + "";
    }
}
