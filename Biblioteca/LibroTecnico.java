/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author Iss
 */
public class LibroTecnico extends Libro {

    public LibroTecnico() {
        super(7);
    }

    public LibroTecnico(String isbn, String titulo) {
        super(7, isbn, titulo);
    }

    public LibroTecnico(String autor, String isbn, String titulo, String genero) {
        super("Libro Tecnico", autor, genero, 7, isbn, titulo);
    }

    @Override
    public String toString() {
        return super.toString() + " Clase: " + this.getClass();
    }

}
