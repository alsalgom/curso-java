/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author Iss
 */
public abstract class Obra {

    public String titulo, isbn;
    public Hashtable<Integer, Socio> historialPrestamo;
    public final int MAXPRESTAMO;
    public boolean enPrestamo;
    public Date fechaPrestamo;

    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public Obra(int MAXPRESTAMO) {
        this.isbn = "";
        this.titulo = "";
        this.historialPrestamo = new Hashtable<Integer, Socio>();
        this.MAXPRESTAMO = MAXPRESTAMO;
        this.enPrestamo = false;

    }

    public Obra(String isbn, String titulo, int MAXPRESTAMO) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.historialPrestamo = new Hashtable<Integer, Socio>();
        this.MAXPRESTAMO = MAXPRESTAMO;
        this.enPrestamo = false;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Hashtable<Integer, Socio> getHistorialPrestamo() {
        return historialPrestamo;
    }

    public void setHistorialPrestamo(Hashtable<Integer, Socio> historialPrestamo) {
        this.historialPrestamo = historialPrestamo;
    }

    public boolean isEnPrestamo() {
        return enPrestamo;
    }

    public void setEnPrestamo(boolean enPrestamo) {
        this.enPrestamo = enPrestamo;
    }

    @Override
    public String toString() {
        return "ISBN: " + this.getIsbn() + ", Titulo: " + this.getTitulo() + ", En Prestamo: " + this.isEnPrestamo() + ", Max Prestamo: " + this.MAXPRESTAMO + " dias , Historial: " + this.getHistorialPrestamo().toString();
    }

}
