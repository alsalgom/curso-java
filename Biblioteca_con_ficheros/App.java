/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Iss
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Controlador conBiblio = null;
        int opcion = 0, SALIR = 0, socio = 0;
        String obra = "";
        String f1 = "";
        Scanner entrada = new Scanner(System.in);
        do {
            System.out.println("1) Fichero texto");
            System.out.println("2) Fichero serializable");
            System.out.println("3) Crear aleatorio");
            System.out.println("4) cargar Obras");
            System.out.println("5) guardar Obras");
            System.out.println("6) cargar Socios");
            System.out.println("7) guardar Socios");
            System.out.println("8) Listar Obras");
            System.out.println("9) Listar Socios");
            System.out.println("10) Listar Obras Prestadas");
            System.out.println("11) Prestar Obra");
            System.out.println("12) Retornar Obra");
            System.out.println("13) Salir");
            opcion = entrada.nextInt();
            switch (opcion) {
                case 1:
                    //fichero texto
                    conBiblio = new Controlador("Texto");
                    
                    break;
                case 2:
                    //fichro serializable
                    conBiblio = new Controlador("Serializable");
                    
                    break;
                case 3:
                    //crear aleatorio
                    conBiblio.crearAleatorios();
                    
                    break;
                case 4:
                    //cargar orbras
                    System.out.println("Nombre de fichero");
                    f1 = entrada.next();
                    conBiblio.cargarObras(f1);
                    break;
                case 5:
                    //guardar obras
                    System.out.println("Nombre de fichero");
                    f1 = entrada.next();
                    conBiblio.guardarObras(f1);
                    break;
                case 6:
                    //cargar socios
                    System.out.println("Nombre de fichero");
                    f1 = entrada.next();
                    conBiblio.cargarSocios(f1);
                    break;
                case 7:
                    //guardar socios
                    System.out.println("Nombre de fichero");
                    f1 = entrada.next();
                    conBiblio.guardarSocios(f1);
                    break;
                case 8:
                    //listar Obras
                    System.out.println(conBiblio.getBiblioteca().imprimirObras());
                    break;
                case 9:
                    //listar Socios
                    System.out.println(conBiblio.getBiblioteca().imprimirSocios());
                    break;
                case 10:
                    //listar obras prestadas
                    conBiblio.obrasNoDevueltas();
                    break;
                case 11:
                    //prestar obra
                    System.out.println("ID Obra");
                    obra = entrada.next();
                    System.out.println("ID Socio");
                    socio = entrada.nextInt();
                    conBiblio.prestarObra(obra, socio);
                    break;
                case 12:
                    //retornar obra
                    System.out.println("ID Obra");
                    obra = entrada.next();
                    System.out.println("ID Socio");
                    socio = entrada.nextInt();
                    conBiblio.retornarObra(obra, socio);
                    break;
                case 13:
                    //salir
                    SALIR = 1;
                    break;
                default:
                    SALIR = 1;
                    break;
            }
            
        } while (SALIR == 0);
        
    }
    
}
