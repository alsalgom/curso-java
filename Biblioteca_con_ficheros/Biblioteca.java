/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Iss
 */
public class Biblioteca {

    private HashMap<String, Socio> socios;
    private HashMap<String, Obra> obras;
    private final int MAXSOCIOS = 50;
    private final int MAXOBRAS = 50;
    private int obrasActuales;
    private int sociosActuales;
    private IGestorBiblioteca gestor;

    public Biblioteca(IGestorBiblioteca gestor) {
        this.socios = new HashMap<String, Socio>();
        this.obras = new HashMap<String, Obra>();
        this.obrasActuales = 0;
        this.sociosActuales = 0;
        this.gestor = gestor;
    }

    public HashMap<String, Socio> getSocios() {
        return socios;
    }

    public void setSocios(HashMap<String, Socio> socios) {
        this.socios = socios;
    }

    public HashMap<String, Obra> getObras() {
        return obras;
    }

    public void setObras(HashMap<String, Obra> obras) {
        this.obras = obras;
    }

    public int getObrasActuales() {
        return obrasActuales;
    }

    public void setObrasActuales(int obrasActuales) {
        this.obrasActuales = obrasActuales;
    }

    public int getSociosActuales() {
        return sociosActuales;
    }

    public void setSociosActuales(int sociosActuales) {
        this.sociosActuales = sociosActuales;
    }

    public IGestorBiblioteca getGestor() {
        return gestor;
    }

    public void setGestor(IGestorBiblioteca gestor) {
        this.gestor = gestor;
    }

    public void cargarObras(String direccion) {
        this.obras = this.gestor.cargarObras(direccion);
    }

    public void guardarObras(String direccion) {
        this.gestor.guardarObras(direccion, this.getObras());
    }

    public void cargarSocios(String direccion) {
        this.socios = this.gestor.cargarSocios(direccion);
    }

    public void guardarSocios(String direccion) {
        this.gestor.guardarSocio(direccion, this.getSocios());
    }

    public void insertarSocio(Persona p) {
        if (p instanceof Socio && this.getSociosActuales() < this.MAXSOCIOS) {
            this.setSociosActuales(this.getSociosActuales() + 1);
            this.socios.put(((Socio) p).getNombre(), (Socio) p);
        } else {
            System.out.println("ERROR");
        }
    }

    public void insertarObra(Obra o) {
        if (o instanceof Obra && this.getObrasActuales() < this.MAXOBRAS) {
            this.setObrasActuales(this.getObrasActuales() + 1);
            this.obras.put(o.getTitulo(), o);
        } else {
            System.out.println("ERROR");
        }
    }

//    public void insertarSocio(Persona p, int pos) {
//        if (p instanceof Socio && this.getSociosActuales() < this.MAXSOCIOS && pos < this.MAXSOCIOS) {
//            if (this.getSocios()[pos] == null) {
//                this.setSociosActuales(this.getSociosActuales() + 1);
//            }
//            this.getSocios()[pos] = p;
//        } else {
//            System.out.println("ERROR");
//        }
//    }
//    public void insertarObra(Obra o, int pos) {
//        if (o instanceof Obra && this.getSociosActuales() < this.MAXOBRAS && pos < this.MAXOBRAS) {
//            if (this.getObras()[pos] == null) {
//                this.setObrasActuales(this.getObrasActuales() + 1);
//            }
//            this.getObras()[pos] = o;
//        } else {
//            System.out.println("ERROR");
//        }
//    }
    public String imprimirSocios() {
        StringBuilder str = new StringBuilder();
        this.socios.values().stream().forEach((s) -> {
            str.append(s.toString());
            str.append("\n");
        });
        return str.toString();
    }

    public String imprimirObras() {
        StringBuilder str = new StringBuilder();
        this.obras.values().stream().forEach((o) -> {
            str.append(o.toString());
            str.append("\n");
        });
        return str.toString();
    }

    public HashSet<Obra> noDevueltos() {
        HashSet<Obra> s = new HashSet<>();

        this.obras.values().stream().forEach((o) -> {
            if (o != null) {
                if (o.isEnPrestamo()) {
                    s.add(o);
                }
            }
        });
        return s;
    }

    public boolean prestarObra(Socio socio, Obra obra) {

        return false;
    }

    public boolean retornarObra(Socio socio, Obra obra) {
        return false;
    }

    @Override
    public String toString() {
        String s = "";
        s = "OBRAS EN LA BIBLIOTECA";
        s = s + this.imprimirObras();
        s = "SOCIOS EN LA BIBLIOTECA";
        s = s + this.imprimirSocios();
        return s;
    }
}
