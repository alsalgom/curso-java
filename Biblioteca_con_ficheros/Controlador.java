/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.HashSet;

/**
 *
 * @author Iss
 */
public class Controlador {
    
    private Biblioteca biblioteca;
    private GeneradorDades genDades;
    private IGestorBiblioteca gestor;
    
    public Controlador(String tipoDatos) {
        switch (tipoDatos) {
            case "Texto":
                gestor = new GestorBibliotecaFichero();
                break;
            case "Serializable":
                gestor = new GestorBibliotecaSerializable();
                break;
            default:
                gestor = new GestorBibliotecaSerializable();
                break;
        }
        this.biblioteca = new Biblioteca(gestor);
        this.genDades = new GeneradorDades();
        
    }
    
    public void crearAleatorios() {
        this.crearSocios();
        this.crearObras();
    }
    
    public Biblioteca getBiblioteca() {
        return biblioteca;
    }
    
    public void setBiblioteca(Biblioteca biblioteca) {
        this.biblioteca = biblioteca;
    }
    
    public void crearSocios() {
        for (int i = 0; i < 20; i++) {
            String nombre = genDades.generarNombre() + " " + genDades.generearApellido();
            int numSocio = genDades.generarNumSocio();
            int telefono = genDades.generarTelefono();
            Socio s = new Socio(nombre, telefono, numSocio);
            biblioteca.insertarSocio(s);
        }
    }
    
    public void crearObras() {
        for (int i = 0; i < 20; i++) {
            String nombre = genDades.generarNombre() + " " + genDades.generearApellido();
            String titulo = genDades.generarTitulos();
            String isbn = titulo.charAt(0) + "" + titulo.charAt(1) + "" + genDades.generarisbn();
            
            int t = (int) (Math.random() * 5);
            switch (t) {
                case 0:
                    Pelicula p = new Pelicula(nombre, isbn, titulo);
                    biblioteca.insertarObra(p);
                    break;
                case 1:
                    String[] c = {"Cuna", "Cdos", "Ctres"};
                    Album a = new Album(nombre, isbn, titulo, c);
                    biblioteca.insertarObra(a);
                    break;
                case 2:
                    Novela n = new Novela(nombre, isbn, titulo, genDades.generarGenero());
                    biblioteca.insertarObra(n);
                    break;
                case 3:
                    LibroTecnico lb = new LibroTecnico(nombre, isbn, titulo, genDades.generarGenero2());
                    biblioteca.insertarObra(lb);
                    break;
            }
            
        }
    }
    
    public void obrasNoDevueltas() {
        int i = 0, j = 0;
        HashSet<Obra> ob = biblioteca.noDevueltos();
        ob.forEach((o) -> {
            System.out.println(o.toString());
        });
    }
    
    public Socio existeSocio(int socio) {
        for (Persona p : biblioteca.getSocios().values()) {
            if (p != null) {
                if (((Socio) p).getNumSocio() == socio) {
                    System.out.println("Socio encontrado");
                    return (Socio) p;
                }
            }
        }
        return null;
    }
    
    public Obra existeObra(String obra) {
        for (Obra o : biblioteca.getObras().values()) {
            if (o != null) {
                if (o.getIsbn().equals(obra)) {
                    System.out.println("obra encontrada");
                    return o;
                }
            }
        }
        return null;
    }
    
    public boolean retornarObra(String obra, int socio) {
        Socio s = this.existeSocio(socio);
        Obra o = this.existeObra(obra);
        if (s != null && o != null) {
            return biblioteca.retornarObra(s, o);
        } else {
            if (s == null) {
                System.out.println("Socio no exite");
            }
            if (o == null) {
                System.out.println("Obra no existe");
            }
        }
        
        return false;
    }
    
    public boolean prestarObra(String obra, int socio) {
        Socio s = this.existeSocio(socio);
        Obra o = this.existeObra(obra);
        if (s != null && o != null) {
            return biblioteca.retornarObra(s, o);
        } else {
            if (s == null) {
                System.out.println("Socio no exite");
            }
            if (o == null) {
                System.out.println("Obra no existe");
            }
        }
        
        return false;
    }

    public void cargarObras(String direccion) {
        this.biblioteca.cargarObras(direccion);
    }
    
    public void guardarObras(String direccion) {
        this.biblioteca.guardarObras(direccion);
    }
    
    public void cargarSocios(String direccion) {
        this.biblioteca.cargarSocios(direccion);
    }
    
    public void guardarSocios(String direccion) {
        this.biblioteca.guardarSocios(direccion);
    }

    @Override
    public String toString() {
        return biblioteca.toString();
    }
    
}
