/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Iss
 */
public class GestorBibliotecaSerializable implements IGestorBiblioteca {

    private InputStream fileIn;
    private InputStream bufferIn;
    private ObjectInput input;
    private OutputStream fileOut;
    private OutputStream bufferOut;
    private ObjectOutput output;

    public GestorBibliotecaSerializable() {
        this.fileIn = null;
        this.bufferIn = null;
        this.input = null;
        this.fileOut = null;
        this.bufferOut = null;
        this.output = null;
    }

    @Override
    public HashMap<String, Obra> cargarObras(String Fichero) {
        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + Fichero + ".bin";
        HashMap<String, Obra> ob = new HashMap<String, Obra>();

        try {
            this.fileIn = new FileInputStream(ruta);
            this.bufferIn = new BufferedInputStream(fileIn);
            this.input = new ObjectInputStream(bufferIn);

            ob = ((HashMap<String, Obra>) input.readObject());
            input.close();
        } catch (ClassNotFoundException | IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                input.close();
            } catch (IOException ex) {
                /**/
            }
        }

        return ob;
    }

    @Override
    public HashMap<String, Socio> cargarSocios(String Fichero) {
        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + Fichero + ".bin";
        HashMap<String, Socio> ob = new HashMap<String, Socio>();

        try {
            this.fileIn = new FileInputStream(ruta);
            this.bufferIn = new BufferedInputStream(fileIn);
            this.input = new ObjectInputStream(bufferIn);

            ob = ((HashMap<String, Socio>) input.readObject());
            input.close();
        } catch (ClassNotFoundException | IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                input.close();
            } catch (IOException ex) {
                /**/
            }
        }

        return ob;
    }

    @Override
    public void guardarObras(String Fichero, HashMap<String, Obra> obras) {
        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + Fichero + ".bin";
        HashMap<String, Obra> ob = obras;
        try {
            this.fileOut = new FileOutputStream(ruta);
            this.bufferOut = new BufferedOutputStream(fileOut);
            this.output = new ObjectOutputStream(bufferOut);
            //for(Alumno alum:alumnos){
            output.writeObject(ob);
            //}
            output.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                output.close();
            } catch (IOException ex) {
                /**/
            }
        }
    }

    @Override
    public void guardarSocio(String Fichero, HashMap<String, Socio> socios) {
        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + Fichero + ".bin";
        HashMap<String, Socio> ob = socios;
        try {
            this.fileOut = new FileOutputStream(ruta);
            this.bufferOut = new BufferedOutputStream(fileOut);
            this.output = new ObjectOutputStream(bufferOut);
            //for(Alumno alum:alumnos){
            output.writeObject(ob);
            //}
            output.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                output.close();
            } catch (IOException ex) {
                /**/
            }
        }
    }

}
