/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.HashMap;

/**
 *
 * @author Iss
 */
public interface IGestorBiblioteca {

    public HashMap<String, Obra> cargarObras(String Fichero);

    public HashMap<String, Socio> cargarSocios(String Fichero);

    public void guardarObras(String Fichero, HashMap<String, Obra> obras);

    public void guardarSocio(String Fichero, HashMap<String, Socio> socios);
}
