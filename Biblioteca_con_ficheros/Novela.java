/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Iss
 */
public class Novela extends Libro {

    public Novela() {
        super(15);
    }

    public Novela(String isbn, String titulo) {
        super(15, isbn, titulo);
    }

    public Novela(String autor, String isbn, String titulo, String genero) {
        super("Novela", autor, genero, 15, isbn, titulo);
    }

    @Override
    public String toString() {
        return super.toString() + " Clase: " + this.getClass();
    }
}
