/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

/**
 *
 * @author Iss
 */
public class Pelicula extends Obra {

    public String director;

    public Pelicula() {
        super(5);
        director = "";
    }

    public Pelicula(String director, String isbn, String titulo) {
        super(isbn, titulo, 5);
        this.director = director;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return super.toString() + ", Director: " + this.getDirector() + " Clase: " + this.getClass();
    }

}
