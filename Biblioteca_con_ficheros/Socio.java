/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteca;

import java.util.Date;

/**
 *
 * @author Iss
 */
public class Socio extends Persona {

    public Date fechaSocio;
    public final int MAXPRESTAMO;
    public int numSocio;
    public int obrasPrestadas;

    public Socio() {
        this.fechaSocio = new Date();
        if (this.fechaSocio.getYear() - new Date().getYear() <= 1) {
            this.MAXPRESTAMO = 2;
        } else {
            this.MAXPRESTAMO = 3;
        }
        this.numSocio = 0;
        this.obrasPrestadas = 0;
    }

    public Socio(String nombre, int telefono, int numSocio) {
        super(nombre, telefono);
        this.fechaSocio = new Date();
        if (this.fechaSocio.getYear() - new Date().getYear() <= 1) {
            this.MAXPRESTAMO = 2;
        } else {
            this.MAXPRESTAMO = 3;
        }
        this.numSocio = numSocio;
        this.obrasPrestadas = 0;
    }

    public Date getFechaSocio() {
        return fechaSocio;
    }

    public void setFechaSocio(Date fechaSocio) {
        this.fechaSocio = fechaSocio;
    }

    public int getNumSocio() {
        return numSocio;
    }

    public void setNumSocio(int numSocio) {
        this.numSocio = numSocio;
    }

    public int getObrasPrestadas() {
        return obrasPrestadas;
    }

    public void setObrasPrestadas(int obrasPrestadas) {
        this.obrasPrestadas = obrasPrestadas;
    }

    @Override
    public String toString() {
        return super.toString() + ", Num. socio: " + this.getNumSocio() + ", Fecha Socio: " + this.getFechaSocio();
    }

}
