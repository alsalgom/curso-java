/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.io.Serializable;

/**
 *
 * @author Iss
 */
public class Alumno implements Serializable {

    private int numAlumno, edad;
    private String nombre, apellido;

    public Alumno() {
        this.nombre = "";
        this.apellido = "";
        this.edad = 0;
        this.numAlumno = 0;

    }

    public Alumno(String nombre, String apellido, int edad, int numAlum) {
        this.edad = edad;
        this.nombre = nombre;
        this.apellido = apellido;
        this.numAlumno = numAlum;
    }

    public int getNumAlumno() {
        return numAlumno;
    }

    public void setNumAlumno(int numAlumno) {
        this.numAlumno = numAlumno;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public String toString() {
        return "Alumno{" + "numAlumno=" + numAlumno + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + '}';
    }

}
