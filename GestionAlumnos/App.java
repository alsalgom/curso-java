/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Iss
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int FIN = 1;
        Scanner entrada = new Scanner(System.in);
        String fichero = "";

        IGestionFicheros gf = null;
        GestionAlumnos ga = null;

        Set<Alumno> alumnos = new HashSet<Alumno>();
        GeneradorDades gd = new GeneradorDades();

        do {
            System.out.println("1> fichero texto");
            System.out.println("2> Serializable");
            System.out.println("3> Cargar");
            System.out.println("4> Guardar");
            System.out.println("5> Crear Aleatorio");
            System.out.println("6> Mostrar");
            System.out.println("9> Salir");

            int opcion = entrada.nextInt();

            switch (opcion) {
                case 1:
                    gf = new GestionFicheroTexto();
                    ga = new GestionAlumnos(gf);
                    break;
                case 2:
                    gf = new GestionFicheroSerializable();
                    ga = new GestionAlumnos(gf);
                    break;
                case 3:
                    System.out.println("NOMBRE DEL FICHERO");
                    fichero = entrada.next();
                    ga.cargarDatos(fichero);
                    break;
                case 4:
                    System.out.println("NOMBRE DEL FICHERO");
                    fichero = entrada.next();
                    ga.guatdarAlumnos(fichero);
                    break;

                case 5:
                    for (int i = 0; i < 10; i++) {
                        String nombre = gd.generarNombre();
                        String apellido = gd.generearApellido();
                        int edad = gd.generarEdad();
                        int numAlum = gd.generarNumAlumno();

                        Alumno a = new Alumno(nombre, apellido, edad, numAlum);

                        alumnos.add(a);
                        ga.darAlta(a);
                    }
                    break;
                case 6:
                    System.out.println(ga.mostrar());
                    break;
                case 9:
                    System.out.println("Salida del programa");
                    FIN = 0;
                    break;
                default:
                    System.out.println("Salida del programa");
                    FIN = 0;
                    break;

            }

        } while (FIN != 0);

    }

}
