/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Iss
 */
public class GestionAlumnos {

    private Set<Alumno> alumnos;
    private IGestionFicheros gestor;

    public GestionAlumnos(IGestionFicheros gestor) {
        this.alumnos = new HashSet<Alumno>();
        this.gestor = gestor;
    }

    public void cargarDatos(String direccion) {
        this.alumnos = this.gestor.cargarAlumnos(direccion);
    }

    public void guatdarAlumnos(String direccion) {
        this.gestor.guardarAlumnos(direccion, this.getAlumnos());
    }

    public Set<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(Set<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public IGestionFicheros getGestor() {
        return gestor;
    }

    public void setGestor(IGestionFicheros gestor) {
        this.gestor = gestor;
    }

    public boolean darAlta(Alumno a) {
        return alumnos.add(a);

    }

    public boolean darBaja(Alumno a) {
        return alumnos.remove(a);
    }

    public String mostrar() {
        StringBuilder str = new StringBuilder();
        this.getAlumnos().stream().forEach((a) -> {
            str.append(a.toString());
            str.append("\n");
        });
        return str.toString();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        this.getAlumnos().stream().forEach((a) -> {
            str.append(a.toString());
        });
        return "GestionAlumnos{" + "alumnos=" + str + '}';
    }

}
