/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;


/**
 *
 * @author Iss
 */
public class GestionFicheroSerializable implements IGestionFicheros {

    private InputStream fileIn;
    private InputStream bufferIn;
    private ObjectInput input;
    private OutputStream fileOut;
    private OutputStream bufferOut;
    private ObjectOutput output;

    public GestionFicheroSerializable() {
        this.fileIn = null;
        this.bufferIn = null;
        this.input = null;
        this.fileOut = null;
        this.bufferOut = null;
        this.output = null;
    }

    @Override
    public Set<Alumno> cargarAlumnos(String Fichero) {
        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + Fichero + ".bin";
        Set<Alumno> alumnos = new HashSet<Alumno>();

        try {
            this.fileIn = new FileInputStream(ruta);
            this.bufferIn = new BufferedInputStream(fileIn);
            this.input = new ObjectInputStream(bufferIn);

            alumnos = ((Set<Alumno>) input.readObject());
            input.close();
        } catch (ClassNotFoundException | IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                input.close();
            } catch (IOException ex) {
                /**/
            }
        }

        return alumnos;

    }

    @Override
    public void guardarAlumnos(String direccion, Set<Alumno> a) {
        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + direccion + ".bin";
        Set<Alumno> alumnos = a;

        try {
            this.fileOut = new FileOutputStream(ruta);
            this.bufferOut = new BufferedOutputStream(fileOut);
            this.output = new ObjectOutputStream(bufferOut);
            //for(Alumno alum:alumnos){
            output.writeObject(alumnos);
            //}
            output.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                output.close();
            } catch (IOException ex) {
                /**/
            }
        }

    }

}
