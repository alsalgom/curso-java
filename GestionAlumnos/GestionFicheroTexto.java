/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionalumnos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Iss
 */
public class GestionFicheroTexto implements IGestionFicheros {

    private File file;
    private FileWriter filewritwe;
    private Scanner scaner;
    private FileReader filereader;
    private BufferedReader buferedreader;

    public GestionFicheroTexto() {
        this.file = null;
        this.filewritwe = null;
        this.scaner = null;
        this.filereader = null;
        this.buferedreader = null;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public FileWriter getFilewritwe() {
        return filewritwe;
    }

    public void setFilewritwe(FileWriter filewritwe) {
        this.filewritwe = filewritwe;
    }

    public Scanner getScaner() {
        return scaner;
    }

    public void setScaner(Scanner scaner) {
        this.scaner = scaner;
    }

    public FileReader getFilereader() {
        return filereader;
    }

    public void setFilereader(FileReader filereader) {
        this.filereader = filereader;
    }

    public BufferedReader getBuferedreader() {
        return buferedreader;
    }

    public void setBuferedreader(BufferedReader buferedreader) {
        this.buferedreader = buferedreader;
    }

    @Override
    public Set<Alumno> cargarAlumnos(String Fichero) {

        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + Fichero + ".txt";
        Set<Alumno> alumnos = new HashSet<Alumno>();

        this.setFile(new File(ruta));

        try {
            this.setScaner(new Scanner(this.getFile()));
        } catch (FileNotFoundException ex) {
            System.out.println("File not Found");
        }

        Scanner s = this.getScaner();
        s.useDelimiter("\n");
        s.useLocale(Locale.ITALIAN);

        while (s.hasNext()) {
            String[] line = s.nextLine().split(";");
            Alumno al = new Alumno(line[1], line[2], Integer.parseInt(line[3]), Integer.parseInt(line[0]));
            alumnos.add(al);
        }

        s.close();
        this.getScaner().close();
        this.setScaner(null);

        return alumnos;
    }

    @Override
    public void guardarAlumnos(String direccion, Set<Alumno> a) {
        String ruta = "C:\\Users\\Iss\\Documents\\NetBeansProjects\\GestionAlumnos\\src\\gestionalumnos\\" + direccion + ".txt";
        Set<Alumno> alumnos = a;

        this.setFile(new File(ruta));
        try {
            this.setFilewritwe(new FileWriter(this.getFile(), false));
        } catch (IOException ex) {
            System.out.println("ERROR");
        }
        FileWriter fw = this.getFilewritwe();
        a.stream().forEach((b) -> {
            try {
                StringBuilder str = new StringBuilder();
                str.append(b.getNumAlumno());
                str.append(";");
                str.append(b.getNombre());
                str.append(";");
                str.append(b.getApellido());
                str.append(";");
                str.append(b.getEdad());
                str.append("\n");
                fw.write(str.toString());
            } catch (IOException ex) {
                System.out.println("ERROR");
            }
        });

        try {
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(GestionFicheroTexto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
