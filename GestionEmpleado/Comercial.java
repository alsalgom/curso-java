/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionempleadosapp;

/**
 *
 * @author Iss
 */
public class Comercial extends Empleado {

    private int comisionVend;
    private int poyectosVend;

    public Comercial() {
        super();
    }

    public Comercial(String nombre, String apellido, String numeroEmpl, int antiguedad) {
        super(nombre, apellido, numeroEmpl, antiguedad);
        this.comisionVend = 1000;
        this.poyectosVend = 0;
        super.calcularSueldo((this.comisionVend * this.poyectosVend));
    }

    public Comercial(String nombre, String apellido, String numeroEmpl) {
        super(nombre, apellido, numeroEmpl);
        this.comisionVend = 1000;
        this.poyectosVend = 0;
        super.calcularSueldo((this.comisionVend * this.poyectosVend));
    }

    public int getComisionVend() {
        return comisionVend;
    }

    public void setComisionVend(int comisionVend) {
        this.comisionVend = comisionVend;
    }

    public int getPoyectosVend() {
        return poyectosVend;
    }

    public void setPoyectosVend(int poyectosVend) {
        this.poyectosVend = poyectosVend;
    }

    @Override
    public String toString() {
        return "Comercial{ " + super.toString() + " comisionVend = " + comisionVend + ", poyectosVend = " + poyectosVend + '}';
    }

    @Override
    public int compareTo(Object o) {

        if (this.getAntiguedad() > ((Empleado) o).getAntiguedad()) {
            return 1;
        } else if (this.getAntiguedad() < ((Empleado) o).getAntiguedad()) {
            return -1;
        } else {
            //mirar por el apellido
            return (this.getApellido().compareTo(((Empleado) o).getApellido()));
        }
    }

}
