/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionempleadosapp;

/**
 *
 * @author Iss
 */
public class Controlador {

    private Empleado empleados[];
    private int numEmpl;
    private final int MAXTRABAJADORES = 20;

    public Controlador() {
        this.empleados = new Empleado[MAXTRABAJADORES];
        this.numEmpl = 0;
        init();
    }

    public Empleado[] getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Empleado[] empleados) {
        this.empleados = empleados;
    }

    public int getNumEmpl() {
        return numEmpl;
    }

    public void setNumEmpl(int numEmpl) {
        this.numEmpl = numEmpl;
    }

    public void init() {
        this.crearEmpleados();
    }

    public void crearEmpleados() {
        Empleado em = null;
        String nombre, apellido, numemp;
        for (int i = 0; i < 12; i++) {
            int r = (int) (Math.random() * (101 - 0)) + 0;
            int r2 = (int) (Math.random() * (4 - 1) + 1);
            switch (r2) {
                case 1:
                    nombre = "Diseñador" + i;
                    apellido = "apellido";
                    numemp = Integer.toString(r) + nombre.charAt(0) + apellido.charAt(0);
                    em = new Diseñador(nombre, apellido, numemp, new String[0]);
                    em.setAntiguedad(r2*(int)(Math.random()*10));
                    this.insertarEmpleado(em, i);
                    break;
                case 2:
                    nombre = "comercial" + i;
                    apellido = "bapellido";
                    numemp = Integer.toString(r) + nombre.charAt(0) + apellido.charAt(0);
                    em = new Comercial(nombre, apellido, numemp);
                    em.setAntiguedad(r2*(int)(Math.random()*10));
                    this.insertarEmpleado(em, i);
                    break;
                case 3:
                    nombre = "desarrollador" + i;
                    apellido = "capellido";
                    numemp = Integer.toString(r) + nombre.charAt(0) + apellido.charAt(0);
                    em = new Desarrollador(nombre, apellido, numemp, new String[0]);
                    em.setAntiguedad(r2*(int)(Math.random()*10));
                    this.insertarEmpleado(em, i);
                    break;
                default:
                    em = null;
                    this.insertarEmpleado(em, i);
                    break;
            }
        }
    }

    public boolean existe(Empleado em) {
        if (this.getNumEmpl() > 0) {
            for (Empleado e : this.getEmpleados()) {
                if (e != null && em.getClass() == e.getClass() && e.equals(em)) {
                    return true;
                }
            }
        }

        return false;
    }

    public void insertarEmpleado(Empleado em, int pos) {
        if (!existe(em)) {
            this.getEmpleados()[pos] = em;
            this.setNumEmpl(this.getNumEmpl() + 1);
        } else {
            System.out.println("empleado ya existente");
        }

    }

    public void ordenarEmpleado() {
        for (int i = 0; i < this.getNumEmpl(); i++) {
            for (int j = 0; j < this.getNumEmpl(); j++) {
                //si es mas grande positivo;si es mas pequeño negativo ;iguales 0
                int iguales = this.getEmpleados()[i].compareTo(this.getEmpleados()[j]);
                if (iguales < 0) {
                    Empleado aux = this.getEmpleados()[i];
                    this.getEmpleados()[i] = this.getEmpleados()[j];
                    this.getEmpleados()[j] = aux;
                }
            }

        }

    }

}
