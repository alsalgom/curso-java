/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionempleadosapp;

/**
 *
 * @author Iss
 */
public class Desarrollador extends Empleado {

    private boolean jefeProyecto;
    private String[] lenguajesConocidos;

    public Desarrollador() {
        super();
    }

    public Desarrollador(String nombre, String apellido, String numeroEmpl, int antiguedad, String[] lenguajesConocidos) {
        super(nombre, apellido, numeroEmpl, antiguedad);
        this.jefeProyecto = false;
        this.lenguajesConocidos = lenguajesConocidos;
        if (jefeProyecto) {
            super.calcularSueldo(3000);
        } else {
            super.calcularSueldo(0);
        }
    }

    public Desarrollador(String nombre, String apellido, String numeroEmpl, String[] lenguajesConocidos) {
        super(nombre, apellido, numeroEmpl);
        this.jefeProyecto = false;
        this.lenguajesConocidos = lenguajesConocidos;

        if (jefeProyecto) {
            super.calcularSueldo(3000);
        } else {
            super.calcularSueldo(0);
        }
    }

    public boolean isJefeProyecto() {
        return jefeProyecto;
    }

    public void setJefeProyecto(boolean jefeProyecto) {
        this.jefeProyecto = jefeProyecto;
    }

    public String[] getLenguajesConocidos() {
        return lenguajesConocidos;
    }

    public void setLenguajesConocidos(String[] lenguajesConocidos) {
        this.lenguajesConocidos = lenguajesConocidos;
    }

    @Override
    public String toString() {
        return "Desarrollador{ " + super.toString() + " jefeProyecto = " + jefeProyecto + ", lenguajesConocidos = " + lenguajesConocidos + '}';
    }

    @Override
    public int compareTo(Object o) {

        if (this.getAntiguedad() > ((Empleado) o).getAntiguedad()) {
            return 1;
        } else if (this.getAntiguedad() < ((Empleado) o).getAntiguedad()) {
            return -1;
        } else {
            //mirar por el apellido
            return (this.getApellido().compareTo(((Empleado) o).getApellido()));
        }
    }

}
