/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionempleadosapp;

/**
 *
 * @author Iss
 */
public class Diseñador extends Empleado {

    private String[] programasConocidos;

    public Diseñador() {
        super();
    }

    public Diseñador(String nombre, String apellido, String numeroEmpl, int antiguedad, String[] programasConocidos) {
        super(nombre, apellido, numeroEmpl, antiguedad);
        this.programasConocidos = programasConocidos;
        super.calcularSueldo(1000 * programasConocidos.length);
    }

    public Diseñador(String nombre, String apellido, String numeroEmpl, String[] programasConocidos) {
        super(nombre, apellido, numeroEmpl);
        this.programasConocidos = programasConocidos;
        super.calcularSueldo(1000 * programasConocidos.length);
    }

    public String[] getProgramasConocidos() {
        return programasConocidos;
    }

    public void setProgramasConocidos(String[] programasConocidos) {
        this.programasConocidos = programasConocidos;
    }

    @Override
    public String toString() {
        return "Dise\u00f1ador{ " + super.toString() + " programasConocidos = " + programasConocidos + '}';
    }

    @Override
    public int compareTo(Object o) {

        if (this.getAntiguedad() > ((Empleado) o).getAntiguedad()) {
            return 1;
        } else if (this.getAntiguedad() < ((Empleado) o).getAntiguedad()) {
            return -1;
        } else {
            //mirar por el apellido
            //si es mas grande positivo;si es mas pequeño negativo ;iguales 0
            return (this.getApellido().compareTo(((Empleado) o).getApellido()));
        }
    }
}
