/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionempleadosapp;

import java.util.Objects;

/**
 *
 * @author Iss
 */
public abstract class Empleado implements Comparable {

    private String nombre;
    private String apellido;
    private String numeroEmpl;
    private int antiguedad;
    private int sueldoBase;
    private final int BASESUELDO = 800;

    public Empleado() {
        this.antiguedad = 0;
        this.sueldoBase = 0;
        this.nombre = "Empleado";
        this.apellido = "Apellido";
        this.numeroEmpl = "100EA";
    }

    public Empleado(String nombre, String apellido, String numeroEmpl, int antiguedad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.numeroEmpl = numeroEmpl;
        this.antiguedad = antiguedad;
        this.sueldoBase = BASESUELDO;
    }

    public Empleado(String nombre, String apellido, String numeroEmpl) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.numeroEmpl = numeroEmpl;
        this.antiguedad = 0;
        this.sueldoBase = BASESUELDO;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNumeroEmpl() {
        return numeroEmpl;
    }

    public void setNumeroEmpl(String numeroEmpl) {
        this.numeroEmpl = numeroEmpl;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public int getSueldoBase() {
        return sueldoBase;
    }

    public void setSueldoBase(int sueldoBase) {
        this.sueldoBase = sueldoBase;
    }

    public void calcularSueldo(int plus) {
        int s = this.getSueldoBase();
        if (getAntiguedad() > 0) {
            s = s + (1000 * this.getAntiguedad());
        }
        if (plus > 0) {
            s = s + plus;
        }
        this.setSueldoBase(s);

    }

    @Override
    public String toString() {
        return "{ " + "nombre = " + nombre + ", apellido = " + apellido + ", numeroEmpl = " + numeroEmpl + ", antiguedad = " + antiguedad + ", sueldoBase = " + sueldoBase + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        return true;
    }

    @Override
    public abstract int compareTo(Object o);

}
