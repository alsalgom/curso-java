/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionempleadosapp;

/**
 *
 * @author Iss
 */
public class GestionEmpleadosApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Controlador con = new Controlador();
        System.out.println("EMPLEADOS ACTUALES");
        for (Empleado e : con.getEmpleados()) {
            if (e != null) {
                System.out.println(e);
            }
        }
        con.ordenarEmpleado();
        System.out.println("\n");
        System.out.println("EMPLEADOS ORDENADOS");
        for (Empleado e : con.getEmpleados()) {
            if (e != null) {
                System.out.println(e);
            }
        }
        

    }

}
