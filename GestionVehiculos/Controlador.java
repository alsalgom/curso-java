/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionvehiculos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import java.util.Set;

/**
 *
 * @author Iss
 */
public class Controlador {

    private IGestorFichero gestor;
    private Set<Vehiculo> vehiculos;
    private Set<Vehiculo> entradasVehiculoNoResidente;
    private Ventana v;
    private boolean isAñadido = false;

    public Controlador() {
        v = new Ventana(this);
        this.gestor = new GestorFicheroTexto();
        this.vehiculos = new HashSet<Vehiculo>();
        this.entradasVehiculoNoResidente = new HashSet<Vehiculo>();
        v.setVisible(true);
        v.iniciarVentana();
    }

    public Controlador(IGestorFichero gestor) {

        this.gestor = gestor;
        this.vehiculos = new HashSet<Vehiculo>();
        this.entradasVehiculoNoResidente = new HashSet<Vehiculo>();
    }

    public IGestorFichero getGestor() {
        return gestor;
    }

    public void setGestor(IGestorFichero gestor) {
        this.gestor = gestor;
    }

    public Set<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(Set<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public Set<Vehiculo> getEntradasVehiculoNoResidente() {
        return entradasVehiculoNoResidente;
    }

    public void setEntradasVehiculoNoResidente(Set<Vehiculo> entradasVehiculoNoResidente) {
        this.entradasVehiculoNoResidente = entradasVehiculoNoResidente;
    }

    public void generarInforme() {
        this.gestor.generarInforme(this.getVehiculos());
    }

    public boolean registroEntrada(String matricula) {
        Controlador.this.isAñadido = false;
        this.vehiculos.stream().forEach((Vehiculo ve) -> {

            if (ve.getMatricula().equals(matricula)) {
                Entrada e = new Entrada();
                System.out.println(e);
                if (ve.addEntrada(e)) {
                    ve.setAparcado(true);
                    Controlador.this.isAñadido = true;
                }
            }
        });

        return Controlador.this.isAñadido;
    }

    public boolean registroSalida(String matricula) {
        Controlador.this.isAñadido = false;
        this.vehiculos.stream().forEach((Vehiculo ve) -> {

            if (ve.getMatricula().equals(matricula)) {

                Date fecha = new Date();
                String s = fecha.getDate() + "/" + ((fecha.getMonth()) + 1) + "/" + fecha.getYear();
                for (Entrada e : ve.getEntradas()) {
                    if (e.getFecha().equals(s) && e.getHoraOut() == null) {
                        e.setHoraOut(fecha);
                        e.calcularPrecio(ve.getPRECIOMINUTO());
                        ve.setAparcado(false);
                        Controlador.this.isAñadido = true;
                    }
                }
            }
        });

        return Controlador.this.isAñadido;
    }

    public boolean registroEntVehiculoNoResidente(String matricula) {

        Vehiculo ve = new VehiculoNoResidente(matricula);
        Entrada en = new Entrada();

        this.entradasVehiculoNoResidente.stream().forEach((Vehiculo v) -> {
            if (ve.equals(v) && !ve.isAparcado()) {

                v.setAparcado(true);
                v.addEntrada(en);
                Controlador.this.isAñadido = true;

            } else {
                Controlador.this.isAñadido = false;
            }
        });
        if (!Controlador.this.isAñadido) {
            ve.setAparcado(true);
            ve.addEntrada(en);
            Controlador.this.isAñadido = this.entradasVehiculoNoResidente.add(ve);

        }
        return Controlador.this.isAñadido;
    }

    public double registroSalVehiculoNoResidente(String matricula) {
        Vehiculo ve = new VehiculoNoResidente(matricula);
        Entrada en = new Entrada();
        double precio = 0.0;
        this.entradasVehiculoNoResidente.stream().forEach((Vehiculo v) -> {
            if (ve.equals(v)) {
                v.setAparcado(false);
                v.getEntradas().stream().forEach((Entrada e) -> {
                    if (e.equals(en) && e.getHoraOut() == null) {
                        e.setHoraOut(new Date());
                        e.calcularPrecio(ve.getPRECIOMINUTO());
                        ve.setAparcado(false);
                    }

                });
            } else {
                ve.setAparcado(false);
                ve.addEntrada(en);
                this.entradasVehiculoNoResidente.add(ve);
            }
        });
        return precio;
    }

    public boolean altaVehiculoOficial(String matricula) {
       
        if (!this.existeVeiculo(matricula)) {

            Vehiculo vo = new VehiculoOficial(matricula);

            this.vehiculos.add(vo);
            return true;

        }
        return false;

    }

    public boolean altaVehiculoResidente(String matricula) {
        
        if (!this.existeVeiculo(matricula)) {

            Vehiculo vr = new VehiculoResidente(matricula);

            this.vehiculos.add(vr);
            return true;

        }
        return false;
    }

    public boolean existeVeiculo(String matricula) {
        Controlador.this.isAñadido = false;
        this.entradasVehiculoNoResidente.forEach((Vehiculo ve)->{
            if (ve.getMatricula().equals(matricula))
                Controlador.this.isAñadido = true;
        });
        this.vehiculos.forEach((Vehiculo ve)->{
            if (ve.getMatricula().equals(matricula))
                Controlador.this.isAñadido = true;
        });
  
        return Controlador.this.isAñadido;
    }

    public void comienzaMes() {
        vehiculos.stream().filter((v) -> (v instanceof VehiculoOficial)).forEach((v) -> {
            v.setEntradas(new ArrayList());
        });
    }

    public void pagosResidentes() {
        this.generarInforme();
    }

}
