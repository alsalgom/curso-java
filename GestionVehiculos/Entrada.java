/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionvehiculos;

import java.util.Date;

/**
 *
 * @author Iss
 */
public class Entrada {

    private Date dia;
    private String fecha;
    private Date horaIn;
    private Date horaOut;
    private long minutosAcumulados;
    private double precioTotal;

    public Entrada() {
        this.dia = new Date();
        this.fecha = dia.getDate() + "/" + ((dia.getMonth())+1) + "/" + dia.getYear();
        this.horaIn = dia;
        this.horaOut = null;
        this.minutosAcumulados = 0;
        this.precioTotal = 0.0;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Date getHoraIn() {
        return horaIn;
    }

    public void setHoraIn(Date horaIn) {
        this.horaIn = horaIn;
    }

    public Date getHoraOut() {
        return horaOut;
    }

    public void setHoraOut(Date horaOut) {
        this.horaOut = horaOut;
        calcularMinutos();
    }

    public long getMinutosAcumulados() {
        return minutosAcumulados;
    }

    public void setMinutosAcumulados(long minutosAcumulados) {
        this.minutosAcumulados = minutosAcumulados;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public void calcularMinutos() {
        this.setMinutosAcumulados(((horaOut.getTime() / 60000) - (dia.getTime() / 60000)));
    }

    public void calcularPrecio(double precio) {
        this.setPrecioTotal(this.getMinutosAcumulados() * precio);
    }

    @Override
    public String toString() {
        return "Entrada{" + "dia=" + dia + ", fecha=" + fecha + ", horaIn=" + horaIn + ", horaOut=" + horaOut + ", minutosAcumulados=" + minutosAcumulados + ", precioTotal=" + precioTotal + '}';
    }
   @Override
    public boolean equals(Object o) {
        if (o instanceof Entrada){
            Entrada en = (Entrada)o;
            return en.getFecha().equals(this.getFecha());
        }
    return false;
    }
}
