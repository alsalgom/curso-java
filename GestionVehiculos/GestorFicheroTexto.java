/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionvehiculos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author Iss
 */
public class GestorFicheroTexto implements IGestorFichero {

    @Override
    public void generarInforme(Set<Vehiculo> vehiculos) {
        File f = null;
        FileWriter fw = null;
        String fichero = "\\informe-" + (new Date()).getYear()+"_"+(((new Date()).getMonth())+1)+"_"+(new Date()).getDate()+".txt";
        File miDir = new File (".");
        
     
        try {
            System.out.println(miDir.getCanonicalPath() + fichero);
            f = new File(miDir.getCanonicalPath() + fichero);
            fw = new FileWriter(f, false);
            
            StringBuilder sb = new StringBuilder();
            sb.append("Matricula        ");
            sb.append("Tiempo estacionamiento (min.)    ");
            sb.append("     Cantidad a pagar ");
            sb.append("\n");

            for (Vehiculo v : vehiculos) {
      
                if (v!=null && v instanceof VehiculoResidente) {
                    sb.append(v.getMatricula());
                    sb.append("               ");
                    int min = 0;
                    double precio = 0;
                    for (Entrada e : v.getEntradas()) {
                        min += e.getMinutosAcumulados();
                        precio += e.getPrecioTotal();
                    }
                    sb.append(min);
                    sb.append("                 ");
                    sb.append(precio);
                    sb.append("\n");
                    
                }

            }
            fw.write(sb.toString());
            fw.close();

        } catch (IOException ex) {
            System.out.println("error de escritura"+ex);
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    /**/
                }
            }
        }
    }

}
