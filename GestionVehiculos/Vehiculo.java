/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionvehiculos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Iss
 */
public abstract class Vehiculo {

    private String Matricula;
    private List<Entrada> entradas;
    private final double PRECIOMINUTO;
    private boolean aparcado;

    public Vehiculo() {
        this.PRECIOMINUTO = 0.02;
        this.entradas = new ArrayList<Entrada>();
        this.aparcado = false;
    }

    public Vehiculo(String Matricula, double PRECIOMINUTO) {
        this.Matricula = Matricula;
        this.PRECIOMINUTO = PRECIOMINUTO;
        this.entradas = new ArrayList<Entrada>();
        this.aparcado = false;
    }

    public String getMatricula() {
        return Matricula;
    }

    public void setMatricula(String Matricula) {
        this.Matricula = Matricula;
    }

    public List<Entrada> getEntradas() {
        return entradas;
    }

    public void setEntradas(List<Entrada> entradas) {
        this.entradas = entradas;
    }

    public boolean isAparcado() {
        return aparcado;
    }

    public void setAparcado(boolean aparcado) {
        this.aparcado = aparcado;
    }

    public boolean addEntrada(Entrada e) {
        return this.entradas.add(e);
    }

    public double getPRECIOMINUTO() {
        return PRECIOMINUTO;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "Matricula=" + Matricula + ", entradas=" + entradas + ", PRECIOMINUTO=" + PRECIOMINUTO + ", aparcado=" + aparcado + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Vehiculo) {
            Vehiculo v = (Vehiculo) o;
            return v.getMatricula().equals(this.getMatricula());
        }
        return false;
    }

}
