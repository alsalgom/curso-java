/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecturafichero;

/**
 *
 * @author Iss
 */
public class GeneradorDades {

    public String[] a = {"el", "la", "lo"}, d = {"y", "de", "desde", "hacia", "donde"},
            o = {"casa", "mesa", "arbol", "bosque", "tren", "avion", "barco"},
            an = {"niño", "niña", "oso", "foca", "perro", "lobo", "gusano"},
            nombres = {"Ferran", "Alonso", "Alan", "Macu", "Luis", "Carla", "Estela", "Brian", "Laura", "Irene", "Sara"},
            apellidos = {"Gomez", "Jimenez", "Fernandez", "Martinez", "Lenon", "Kennedy", "Obama", "Müller", "Rowlling"},
            generos = {"Ficcion", "Misterio", "Thriller", "Accion", "Infantil", "Romantico", "autobiogradico", "Historico"},
            generos2 = {"Tecnico", "Matematicas", "Quimica", "Fisica", "Arquitectura", "Java", "Programacion", "Calculo"};

    public GeneradorDades() {
    }

    public String generarNombre() {
        return nombres[(int) (Math.random() * nombres.length)];
    }

    public String generearApellido() {
        return apellidos[(int) (Math.random() * apellidos.length)];
    }

    public String generarTitulos() {
        String titulo = "";
        titulo += a[(int) (Math.random() * a.length)] + " ";
        titulo += an[(int) (Math.random() * an.length)] + " ";
        titulo += d[(int) (Math.random() * d.length)] + " ";
        titulo += a[(int) (Math.random() * a.length)] + " ";
        titulo += o[(int) (Math.random() * o.length)];
        return titulo;
    }

    public int generarTelefono() {
        String t = "";
        for (int i = 0; i < 9; i++) {
            t = t + ((int) (Math.random() * 9));
        }
        int p = Integer.parseInt(t);
        return p;
    }

    public int generarNumSocio() {
        String t = "";
        for (int i = 0; i < 9; i++) {
            t = t + ((int) (Math.random() * 9));
        }
        int p = Integer.parseInt(t);
        return p;
    }

    public String generarisbn() {
        String t = "";
        for (int i = 0; i < 5; i++) {
            t = t + ((int) (Math.random() * 9));
        }
        int p = Integer.parseInt(t);
        return t;
    }

    public String generarGenero() {
        return generos[(int) (Math.random() * generos.length)];
    }

    public String generarGenero2() {
        return generos2[(int) (Math.random() * generos2.length)];
    }

    public String generarFecha(int año) {
        return "";
    }
}
