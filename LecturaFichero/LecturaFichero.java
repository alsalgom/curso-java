/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lecturafichero;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Iss
 */
public class LecturaFichero {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LecturaFichero lf = new LecturaFichero();
        lf.lecturaFichero1();
        lf.lecturaFichero2();
        lf.lecturaFichero3();
        lf.escrituraFichero();

    }

    public void lecturaFichero1() {
        FileReader reader = null;
        BufferedReader br = null;
        String linia;
        String[] lf = null;
        try {

            reader = new FileReader("C:\\Users\\Iss\\Documents\\NetBeansProjects\\LecturaFichero\\src\\lecturafichero\\notas1.txt");

            br = new BufferedReader(reader);

            double media = 0;
            while ((linia = br.readLine()) != null) {
                lf = linia.split("-");
            }

            for (String lf1 : lf) {
                media = Double.parseDouble(lf1) + media;
            }
            media = media / lf.length;
            br.close();
            System.out.println(media);
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {/**/
                }
            }
        }
    }

    public void lecturaFichero2() {
        FileReader reader = null;
        BufferedReader br = null;
        String linia;
        String[] lf = null;
        int i = 0;
        try {

            reader = new FileReader("C:\\Users\\Iss\\Documents\\NetBeansProjects\\LecturaFichero\\src\\lecturafichero\\notas2.txt");

            br = new BufferedReader(reader);

            double media = 0;
            while ((linia = br.readLine()) != null) {
                media += Double.parseDouble(linia);
                i++;

            }
            media = media / i;
            br.close();
            System.out.println(media);
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {/**/
                }
            }
        }
    }

    public void lecturaFichero3() {
        File reader = null;
        Scanner s = null;
        double notas = 0;
        int i = 0;
        try {

            reader = new File("C:\\Users\\Iss\\Documents\\NetBeansProjects\\LecturaFichero\\src\\lecturafichero\\notas3.txt");
            s = new Scanner(reader);
            s.useDelimiter("\n");
            s.useLocale(Locale.US);
            while (s.hasNext()) {

                notas += Double.parseDouble(s.nextLine());
                i++;

            }
            notas = notas / i;
            System.out.println(notas);
            s.close();
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } finally {
            if (s != null) {
                s.close();
            }

        }
    }

    public void escrituraFichero() {
        File f = null;
        FileWriter fw = null;
        GeneradorDades gd = new GeneradorDades();
        try {
            f = new File("C:\\Users\\Iss\\Documents\\NetBeansProjects\\LecturaFichero\\src\\lecturafichero\\nombres.txt");
            fw = new FileWriter(f, false);
            for(int i =0;i<12;i++){
                String nombre = gd.generarNombre(),
                       apellido1 = gd.generearApellido(),
                       apellido2 = gd.generearApellido();
                fw.write(nombre+";"+apellido1+";"+apellido2+"\n");
            }
            fw.close();
           
        } catch (IOException ex) {
            /**/
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    /**/
                }
            }
        }

    }
    /*public List<double> obtenerNotas(String linea) {
        List<double> nums = new ArrayList<double>();
        if (linea.contains(" ")) {
            String[] numsStr = linea.split(" ");
            for (int i = 0; i < numsStr.length; i++) {
                nums.add(Double.parseDouble(numsStr[i]));
            }
        }
        return nums;
    }*/
}
