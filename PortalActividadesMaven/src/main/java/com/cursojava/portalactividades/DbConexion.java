/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cursojava.portalactividades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Iss
 */
public class DbConexion {

    private Connection con;
    private String bdurl;
    private String bdusr;
    private String bdpass;
    private Statement st;
    private ResultSet resultado;
    private String consulta;
    private PreparedStatement pst;

    public DbConexion(String bdurl) {
        this.bdusr = "root";
        this.bdpass = "cursojava";
        this.bdurl = bdurl;
        this.con = null;
        this.st = null;
        this.resultado = null;
        this.consulta = "";

    }

    public boolean abrirConexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.con = DriverManager.getConnection(bdurl, bdusr, bdpass);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Connection obtenerConexion() {
        return this.getCon();
    }

    public void conectar() {
        abrirConexion();
    }

    public boolean ejecSql(String sql) {
        this.setConsulta(sql);
        if (con != null) {
            try {
                st = con.createStatement();
                resultado = st.executeQuery(this.consulta);

                return true;

            } catch (SQLException ex) {
                return false;
            }

        }
        return false;
    }

    public boolean ejecSqlps(String sql, int numParam, String[] params) {
        this.setConsulta(sql);
        if (con != null) {
            try {
                pst = con.prepareStatement(sql);
                if (numParam == 2) {
                    pst.setString(1, params[0]);
                    pst.setString(2, params[1]);
                } else {
                    pst.setString(1, params[0]);
                    pst.setString(2, params[1]);
                    pst.setString(3, params[2]);
                    pst.setString(4, params[3]);
                }
                System.out.println("SQL " + pst.toString());
                resultado = pst.executeQuery();

                return true;

            } catch (SQLException ex) {
                ex.printStackTrace();
                return false;
            }

        }
        return false;
    }

    public boolean ejecSqlps(PreparedStatement pst1) {

        if (con != null && pst1 != null) {
            try {
                this.pst = pst1;

                resultado = pst.executeQuery();
                return true;

            } catch (SQLException ex) {
                return false;
            }

        }
        return false;
    }

    public void cierre() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (resultado != null) {
            try {
                resultado.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void validar() {
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String getBdurl() {
        return bdurl;
    }

    public void setBdurl(String bdurl) {
        this.bdurl = bdurl;
    }

    public String getBdusr() {
        return bdusr;
    }

    public void setBdusr(String bdusr) {
        this.bdusr = bdusr;
    }

    public String getBdpass() {
        return bdpass;
    }

    public void setBdpass(String bdpass) {
        this.bdpass = bdpass;
    }

    public Statement getSt() {
        return st;
    }

    public void setSt(Statement st) {
        this.st = st;
    }

    public ResultSet getResultado() {
        return resultado;
    }

    public void setResultado(ResultSet resultado) {
        this.resultado = resultado;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

}
