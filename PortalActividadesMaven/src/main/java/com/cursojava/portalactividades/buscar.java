/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cursojava.portalactividades;

import com.cursojava.portalactividades.modelos.Curso;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Iss
 */
@WebServlet(
        name = "buscar",
        urlPatterns = {"/buscar"},
        initParams = @WebInitParam(
                name = "bdUrl",
                value = "jdbc:mysql://localhost:3306/actividades"
        )
)
public class buscar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private DbConexion con = null;
    private String sql = "";
    private List<Curso> cursos = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String bd = getInitParameter("bdUrl");
        this.con = new DbConexion(bd);
        this.con.abrirConexion();

        String key = request.getParameter("searchKey");
        String tipo = request.getParameter("tipoActividad");
        String horario = request.getParameter("horarioActividad");
        String[] results = request.getParameterValues("aplicarFiltros");
        int numparams = 0;
        String[] params = {"%" + key + "%", "%" + key + "%", tipo, horario};
        if (results == null) {
            this.sql = "select * from actividades.cursos where nombre like ? or description like ?;";
            numparams = 2;
        } else {
            this.sql = "select * from actividades.cursos where (nombre like ? or description like ?) and (tipo = ? or horario = ?);";
            numparams = 4;
        }

        if (this.con.ejecSqlps(this.sql, numparams, params)) {
            
            ResultSet res = this.con.getResultado();
            this.cursos = new ArrayList<Curso>();
            try {
                while (res.next()) {
                    int id = res.getInt("idcursos");
                    String nombre = res.getString("nombre");
                    String descripcion = res.getString("description");
                    String dia = res.getString("dia");
                    String time = res.getString("horario");
                    Date fechaInicio = res.getDate("fechainicio");
                    Date fechaFin = res.getDate("fechafin");
                    String type = res.getString("tipo");
                    int plazas = res.getInt("plazas");
                    int plazasLibres = res.getInt("plazaslibres");
                    int precio = res.getInt("precio");
                    Curso curso = new Curso(id, nombre, descripcion, dia, time, fechaInicio, fechaFin, type, plazas, plazasLibres, precio);
                    this.cursos.add(curso);
                    System.out.println(curso.toString());

                }
            } catch (SQLException ex) {
                Logger.getLogger(buscar.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("busqueda", key);
            request.setAttribute("resultado", this.cursos);
            
            System.out.println("ejecutado");
            request.getRequestDispatcher("./cursos.jsp").forward(request, response);
        } else {
            System.out.println("error en la consulta");
            response.sendRedirect("./error.jsp");
        }

      
        this.con.cierre();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
