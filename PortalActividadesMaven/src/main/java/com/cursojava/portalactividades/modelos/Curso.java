/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cursojava.portalactividades.modelos;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Iss
 */
public class Curso {

    private int id;
    private String nombre;
    private String descripcion;
    private String dia;
    private String horario;
    private Date fechaInicio;
    private Date fechaFin;
    private String tipo;
    private int plazas;
    private int plazasLibres;
    private int precio;
    private boolean libre;

    public Curso() {
        this.id = 0;
        this.nombre = "";
        this.descripcion = "";
        this.dia = "";
        this.horario = "";
        this.fechaInicio = null;
        this.fechaFin = null;
        this.tipo = "";
        this.plazas = 0;
        this.plazasLibres = 0;
        this.precio = 0;
        this.libre = false;
    }

    public Curso(int id, String nombre, String descripcion, String dia, String horario, Date fechaInicio, Date fechaFin, String tipo, int plazas, int plazasLibres, int precio) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.dia = dia;
        this.horario = horario;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.tipo = tipo;
        this.plazas = plazas;
        this.plazasLibres = plazasLibres;
        this.precio = precio;
        if (plazasLibres > 0 && plazasLibres < plazas) {
            this.libre = true;
        } else {
            this.libre = false;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPlazas() {
        return plazas;
    }

    public void setPlazas(int plazas) {
        this.plazas = plazas;
    }

    public int getPlazasLibres() {
        return plazasLibres;
    }

    public void setPlazasLibres(int plazasLibres) {
        this.plazasLibres = plazasLibres;
    }

    public boolean isLibre() {
        return libre;
    }

    public void setLibre(boolean libre) {
        this.libre = libre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Curso other = (Curso) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Curso{" + "id=" + id + ", nombre=" + nombre + ", descripcion=" + descripcion + ", dia=" + dia + ", horario=" + horario + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", tipo=" + tipo + ", plazas=" + plazas + ", plazasLibres=" + plazasLibres + ", libre=" + libre + '}';
    }

}
