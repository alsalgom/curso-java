<%-- 
    Document   : index
    Created on : 21-oct-2016, 8:49:08
    Author     : Iss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Portal Actividades: Buscar</title>
        <link rel="stylesheet" type="text/css" href="css.css">

        <script   src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
        <script type="text/javascript">
            //<![CDATA[
            $(document).ready(function () {
                $("#aplicarFiltros").on("change", function () {
                    //$(this:checked).alert("selected");
                });

            });

            //]]>
        </script>
    </head>
    <body>
        <h1>Buscador de actividades</h1>
        <form action="./buscar" method="post">
            <table class="formTable" id="buscarClave">
                <thead>
                    <tr>
                        <th>Introduce la palabra clave para buscar la actividad:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" name="searchKey" id="searchKey" required/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="Buscar" title="text" id="searchButton" /></td>
                    </tr>
                </tbody>
            </table>
            <table class="formTable" id="buscarFiltros">
                <thead>
                    <tr>
                        <th colspan="2">Elije filtros para la busqueda</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Tipo: 
                        </td>
                        <td>
                            <select name="tipoActividad" id="tipoActividad">
                                <option value="Deportiva">Deportiva</option>
                                <option value="Cultural">Cultural</option>
                                <option value="Gastronomica">Gastronomica</option>
                                <option value="Musical">Musical</option>
                                <option value="Curso">Curso</option>
                                <option value="Otro">Otro</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Horario:
                        </td>
                        <td>
                            <select name="horarioActividad" id="horarioActividad">
                                <option value="8-10">8-10</option>
                                <option value="10-12">10-12</option>
                                <option value="12-14">12-14</option>
                                <option value="14-16">14-16</option>
                                <option value="16-18">16-18</option>
                                <option value="18-20">18-20</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="checkbox" name="aplicarFiltros" id ="aplicarFiltros" value="Aplicar filtros"/>Aplicar filtros</td>
                    </tr>
                </tbody>
            </table>
        </form>
        <a href=".index.html">Volver al inicio</a>
    </body>
</html>
