<%-- 
    Document   : cursos
    Created on : 21-oct-2016, 11:12:46
    Author     : Iss
--%>

<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.List"%>
<%@page import="com.cursojava.portalactividades.modelos.Curso"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Portal Actividades: Cursos</title>
        <link rel="stylesheet" type="text/css" href="css.css">

        <script   src="https://code.jquery.com/jquery-3.1.1.js"   integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
        <script type="text/javascript">
            //<![CDATA[
            $(document).ready(function () {


            });

            //]]>
        </script>
    </head>
    <body>
        <%
            List<Curso> cursos = (ArrayList) request.getAttribute("resultado");
            String key = (String) request.getAttribute("busqueda");
            if (cursos == null || cursos.isEmpty()) {
                response.sendRedirect("index.jsp");
            }
            session.setAttribute("resultados", cursos);
            

        %>
        <h3>Busqueda relacionadas con: <%out.println("'" + key + "'");%></h3>
        <table>
            <thead>
                <tr><th collspan="4">Resultados</th></tr>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Tipo</th>
                </tr>
            </thead>
            <tbody>
                <% for (Curso cur : cursos) {%>
                <tr>
                    <td><a href="./descripcion.jsp?id=<%out.println(cur.getId());%>"><%out.println(cur.getId());%></a></td>
                    <td><%out.println(cur.getNombre());%></td>
                    <td><%out.println(cur.getDescripcion());%></td>
                    <td><%out.println(cur.getTipo());%></td>
                </tr>  
                <%  }%>
            </tbody>
        </table>
        <a href="./index.html">Volver al inicio</a>
    </body>
</html>
