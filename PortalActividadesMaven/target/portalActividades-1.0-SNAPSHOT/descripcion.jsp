<%-- 
    Document   : descripcion
    Created on : 24-oct-2016, 8:49:34
    Author     : Iss
--%>

<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.cursojava.portalactividades.DbConexion"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cursojava.portalactividades.modelos.Curso"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Portal de actividades, error</title>
        <link rel="stylesheet" type="text/css" href="css.css">

        <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="   crossorigin="anonymous"></script>
        <script type="text/javascript">
            //<![CDATA[
            $(document).ready(function () {
                $(".formreserva").hide();
                $('.reservar').click(function () {
                    if (this.checked) {
                        $(".formreserva").show();
                    } else {
                        $(".formreserva").hide();
                    }
                });

            });
            //]]>
        </script>
        <%
            int id = Integer.parseInt(request.getParameter("id"));
            session.setAttribute("id", id);
            List<Curso> cursos = (ArrayList) session.getAttribute("resultados");
            Curso curs = null;
            if (cursos == null || id == 0) {
                response.sendRedirect("./index.jsp");

            }
            for (Curso c : cursos) {
                if (c.getId() == id) {
                    curs = c;
                }
            }

        %>
    </head>
    <body>
        <h1>Decripcion del curso</h1>
        <%            if (curs == null) {
                response.sendRedirect("index.jsp");
            }
        %>
        <div class="resultado">
            <p>ID: <span><% out.println(curs.getId());%></span></p>
            <p>Nombre: <span><% out.println(curs.getNombre());%></span></p>
            <p>Descripcion: <span><% out.println(curs.getDescripcion());%></span></p>
            <p>Dia: <span><% out.println(curs.getDia());%></span></p>
            <p>Horario: <span><% out.println(curs.getHorario());%></span></p>
            <p>Fecha inicio: <span><% out.println(curs.getFechaInicio());%></span></p>
            <p>Fecha fin: <span><% out.println(curs.getFechaFin());%></span></p>
            <p>Tipo: <span><% out.println(curs.getTipo());%></span></p>
            <p>Plazas: <span><% out.println(curs.getPlazas());%></span></p>
            <p>Plazas libres: <span><% out.println(curs.getPlazasLibres());%></span></p>
            <p>Precio: <span><% out.println(curs.getPrecio());%></span></p>
            <p>Libre: <span><% out.println(curs.isLibre());%></span></p>
            <span><input type="checkbox" class="reservar" name="reservar" value="reserva">Reservar</span>
            <div class="formreserva">
                <form method="post" action="./reserva">
                    Nombre:<input type="text" name="nombre"/><br/>
                    Apellidos:<input type="text" name="apellido"/><br/>
                    Direccion:<input type="text" name="direccion"/><br/>
                    Telefono:<input type="text" name="telefono"/><br/>
                    Email:<input type="text" name="email"/><br/>
                    <input type="submit" name="reserva" value="reserva"/>
                </form>
                

            </div>
        </div>



    </body>
</html>
