DROP SCHEMA IF EXISTS almacen;
Create schema if not exists almacen;
use  almacen;
create table if not exists almacen.almacenes(
	codigo int unique,
    Lugar nvarchar(100),
    Capacidad int,
    primary key(codigo)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
create table if not exists almacen.cajas(
		NumRef char(5) unique,
		Contenido nvarchar(100),
		Valor int,
		almacen int,
		primary key (NumRef),
		FOREIGN KEY (`almacen`)
		REFERENCES `almacen`.`almacenes` (`codigo`)
		on update cascade
		on delete cascade
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

insert into almacenes values(1234,"Barcelona",200);
insert into almacenes values(2345,"Girona",100);
insert into almacenes values(3425,"Bilbao",250);
insert into almacenes values(3215,"Tarragona",80);
insert into almacenes values(4533,"Valencia",150);
insert into almacenes values(6432,"Hospitalet",50);
insert into cajas values(902,"calculadora",30,3215);
insert into cajas values(345,"televison",50,1234);
insert into cajas values(342,"radio",80,4533);
insert into cajas values(657,"pc's",200,6432);
insert into cajas values(423,"raton",100,3215);
insert into cajas values(654,"teclados",130,1234);
insert into cajas values(678,"patatas",90,3425);
insert into cajas values(324,"lapizes",10,4533);
insert into cajas values(967,"libros",50,1234);



select * from almacenes;
select * from cajas where cajas.Valor >150;
select distinct cajas.NumRef,cajas.Contenido from cajas ;
select avg(Valor) from cajas;
select almacenes.codigo,avg(cajas.Valor) from cajas,almacenes where cajas.almacen = almacenes.codigo group  by cajas.almacen;
select almacenes.codigo,avg(cajas.Valor) as ValorMed from almacenes,cajas where cajas.almacen = almacenes.codigo group by cajas.almacen having ValorMed >150  ;
select cajas.NumRef,almacenes.Lugar from cajas,almacenes where cajas.almacen = almacenes.codigo;
select cajas.almacen,count(cajas.NumRef) from cajas group by cajas.almacen;
select codigo from almacenes where Capacidad<(select count(*) from cajas where almacen = codigo);
select cajas.NumRef,almacenes.Lugar from cajas,almacenes where cajas.almacen = almacenes.codigo and almacenes.Lugar = "Bilbao";
insert into almacenes values(2,"Barcelona",3);
insert into cajas values("H5RT","papel",200,2);
update cajas set cajas.Valor = cajas.Valor-((cajas.Valor*15)/100) where cajas.Valor is not null;
update cajas set valor = valor*0.80 where valor >(select avg(valor) from cajas);
delete from cajas where cajas.Valor<100;
delete from cajas where almacen in (select codigo from almacenes where capacidad <(select count(*) from cajas where Almacen = codigo));