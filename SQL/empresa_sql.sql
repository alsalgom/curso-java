DROP SCHEMA IF EXISTS empresa;
Create schema if not exists empresa;
use  empresa;
create table if not exists empresa.departamentos(
	codigo int,
    Nombre nvarchar(100),
    presupuesto int,
    primary key(codigo)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
create table if not exists empresa.empleados(
		dni varchar(8) unique,
		nombre nvarchar(100),
		apellidos nvarchar(255),
		departamento int,
		primary key (dni),
		FOREIGN KEY (`departamento`)
		REFERENCES `empresa`.`departamentos` (`codigo`)
		on update cascade
		on delete cascade
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

insert into departamentos values(1,"dep1",150);
insert into departamentos values(2,"dep2",160);
insert into departamentos values(3,"dep3",50);
insert into departamentos values(4,"dep4",140);
insert into departamentos values(14,"dep4",10);
insert into departamentos values(17,"dep4",15);
insert into departamentos values(77,"dep4",180);
insert into empleados values(4921,"Pepito","Martinez",1);
insert into empleados values(3453,"Juan","Jimenez",2);
insert into empleados values(2413,"Luis","Gomez",3);
insert into empleados values(5634,"Fran","Alonso",77);
insert into empleados values(4145,"Sara","Perez",4);
insert into empleados values(6865,"Luisa","Lopez",1);
insert into empleados values(6754,"Pau","Perez",14);
insert into empleados values(2345,"Paco","Lopez",77);
insert into empleados values(4352,"Miquel","Maestre",17);

SELECT apellidos from empresa.empleados;
SELECT apellidos from empresa.empleados group by apellidos;
SELECT distinct apellidos from empresa.empleados;
select * from empresa.empleados where apellidos = "Lopez";
select * from empresa.empleados where apellidos in ("Lopez","Perez");
select * from empresa.empleados where departamento = 14;
select * from empresa.empleados where departamento in (17,77);
select * from empresa.empleados where apellidos like "P%";
select sum(departamentos.presupuesto) from empresa.departamentos;

select empleados.departamento,count(*)
	from empresa.empleados
	group by empleados.departamento;

select * from empresa.empleados,empresa.departamentos 
	where empleados.departamento = departamentos.codigo;

select empleados.dni,empleados.nombre,empleados.apellidos,departamentos.Nombre,departamentos.presupuesto 
	from empresa.empleados,empresa.departamentos 
	where empleados.departamento = departamentos.codigo;

select empleados.nombre,empleados.apellidos 
	from empleados,departamentos 
	where empleados.departamento = departamentos.codigo and departamentos.presupuesto>60;

select * from empresa.departamentos 
	having departamentos.presupuesto > (select avg(departamentos.presupuesto) 
	from empresa.departamentos);
	
select departamentos.Nombre,count(empleados.nombre) as NumEmpl
	from empresa.empleados,empresa.departamentos 
	where departamentos.codigo = empleados.departamento by empleados.departamento;

