DROP SCHEMA IF EXISTS proveedores;
Create schema if not exists proveedores;
use  proveedores;
create table if not exists proveedores(
	codigo int unique,
    Nombre nvarchar(100),
    primary key(codigo)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
create table if not exists suministra(
		codigopieza int unique,
		idproveedor char(4),
		precio int,
		primary key (codigopieza),
		primary key (idproveedor),
		FOREIGN KEY (`codigopieza`)
		REFERENCES `proveedores`.`piezas` (`id`)
		on update cascade
		on delete cascade,
		FOREIGN KEY (`idproveedor`)
		REFERENCES `proveedores`.`proveedores` (`codigo`)
		on update cascade
		on delete cascade
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
create table if not exists piezas(
		id char(4) unique,
		nombre nvarchar(100),
		primary key (id),
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


select nombre from piezas;
select * from proveedores;
select codigopieza, avg(precio) from suministra group by codigopieza;
select proveedores.nombre from proveedores inner join suministra on proveedores.id = suministra.idproveedor and suministra.codigopieza= 1;
select piezas.nombre from piezas inner join suministra on piezas.codigo = suministra.codigopieza and suministra.idproveedor = 'HAL';
select p1.nombre, pr1.nombre, precio from piezas p1 inner join (suministra s1 inner join proveedores pr1 on s1.idproveedor = pr1.id)on p1.codigo = s1.codigopieza where precio in (select max(precio)from suministra s2 group by s2.codigopieza having s2.codigopieza = p1.codigopieza);
insert into suministra values("TNBC",1,7);
update suminsitra set Precio = precio +1;
delete from suministra where idproveedor = "RBT";
	delete from suministra where idproveedor = "RBT" and codigopieza= 4;