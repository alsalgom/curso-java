CREATE DATABASE tiendainfor;
USE tiendainfor;
DROP TABLE articulos;
DROP TABLE fabricantes;

CREATE TABLE fabricantes(
	codigo int unique,
	nombre nvarchar(100),
	PRIMARY KEY(codigo)
);
CREATE TABLE articulos(
	codigo int unique,
	nombre nvarchar(100),
	precio int,
	fabricante int,
	PRIMARY KEY (codigo),
	FOREIGN KEY(fabricante)
	REFERENCES fabricantes(codigo)
	ON UPDATE CASCADE
	ON DELETE CASCADE
);
select nombre from articulos;
select nombre,precio from articulos;
select nombre from articulos where precio<=200;
select * from articulos where precio between 60 and 120;
select nombre,precio*166,386 as precio_pst from articulos ;


select avg(precio) as precmedio from articulos;
select avg(precio) as precmedio from articulos where fabricante =2;
select count(articulos) from articulos where precio >= 180;
select nombre,precio  from articulos where precio >= 180 order by precio DESC,nombre ASC ;
select nombre,precio  from articulos where precio >= 180 order by precio DESC ;
select nombre,precio from articulos where precio >= 180 order by nombre ASC ;


select * from articulos as a,fabricante as f where a.fabricante = f.codigo;
select articulos.nombre,articulos.precio,fabricnate.nombre from articulos, fabricante where articulos.fabricante = fabricante.codigo
select avg(precio),fabricante from articulos group by fabricante;


