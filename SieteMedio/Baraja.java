/**
 * 
 */
package sieteMedio;

import java.util.*;

/**
 * @author Iss
 *
 */
import sieteMedio.Carta;
public class Baraja {

	/**
	 * 
	 * 
	 */
	private Carta[] baraja = null;
	private final int MAXBARAJA = 10;
	private int cartasBaraja = 0;
	
	public Baraja() {
		// TODO Auto-generated constructor stub
		//se cra una nueva baraja
		setBaraja(new Carta[MAXBARAJA]);
		//se crean las cartas y se ponen en la baraja
		setBaraja(createCartas());
		//se mezcla la pila
		mezclar(getBaraja());
		cartasBaraja = MAXBARAJA;
	};
	
	public Carta[] getBaraja(){
		return this.baraja;
	};
	
	public void setBaraja(Carta c,int pos){
		this.baraja[pos] = c;
	};
	
	public void setBaraja(Carta baraja[]){
		this.baraja = baraja;
	};
	
	public Carta getCarta(){
		if(hayCartas()){
			Carta c = null;
			int pos = (int)(Math.random()*10);
			while (pos>=this.cartasBaraja){pos = (int)(Math.random()*10);};
			c = this.getBaraja()[pos];
			return c;
		}else{
			return null;
		}
	};
	
	public void eliminarCarta(Carta carta){
		//elimina una carta de la pila
		if (hayCartas()){
			ArrayList<Carta> a = new ArrayList<>(Arrays.asList(this.baraja));
			int pos = 0;
			for(int i= 0; i <a.size();i++){
				if (getBaraja()[i].getNombre().equals(carta.getNombre()) && getBaraja()[i].getValor() == carta.getValor()){
					pos = i;
				}
			}
			a.remove(pos);
			
			Carta[] b = new Carta[a.size()];
			a.toArray(b);
			setBaraja(b);
			cartasBaraja -=1 ;
		}else{
			System.out.println(new String("No hay cartas a eliminar"));
		}
	};
	
	public Carta[] createCartas(){
		//crea de nuevo las cartas
		Carta[] c = {
				new Carta("uno",1),new Carta("dos",2),
				new Carta("tres",3), new Carta("cuatro",4), 
				new Carta("cinco",5), new Carta("seis",6), 
				new Carta("siete",7),new Carta("sota",0.5),
				new Carta("caballo",0.5), new Carta("rei",0.5)
				};
		return c;
	};
	
	public Carta[] mezclar(Carta[] a){
		//recibe la pila y mezla las cartas y retorna la pila mezclada
		   int m= a.length-1;
		   for (int i=m;i>1;i--){ 
		      int alea=(int) Math.floor(i*Math.random()); 
		      Carta temp=a[i]; 
		       a[i]=a[alea]; 
		       a[alea]=temp; 
		    }
		   return a;
	};
	
	public void mezcla(){
		//mezcla las cartas de la pila ya creada
		Carta[] a = getBaraja();
		int m= a.length-1;
		   for (int i=m;i>1;i--){ 
		      int alea=(int) Math.floor(i*Math.random()); 
		      Carta temp=a[i]; 
		       a[i]=a[alea]; 
		       a[alea]=temp; 
		    }
		  setBaraja(a);
	};
	
	public boolean hayCartas(){
		if(cartasBaraja >=1) return true;
		else return false;
	};
	
	public String toString(){
		String a = new String("");
		for (Carta carta:getBaraja()){
			a+= new String(carta.toString()+"\n");
		};
		return new String("Baraja: \n"+a);
	};
	

}
