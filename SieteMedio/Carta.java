/**
 * 
 */
package sieteMedio;

/**
 * @author Iss
 *
 */
public class Carta {

	/**
	 * 
	 */
	private String nombre;
	private double valor;
	
	public Carta(String nombre, double valor) {
		// TODO Auto-generated constructor stub
		this.nombre = nombre;
		this.valor = valor;
	};
	
	public String getNombre(){
		return this.nombre;
	};
	
	public void setNombre(String nombre){
		this.nombre = new String(nombre);
	};
	
	public double getValor(){
		return this.valor;
	};
	public void setValor(int valor){
		this.valor = new Integer(valor);
	};
	public String toString(){
		return new String("Carta: "+getNombre()+" Valor: "+getValor());
	};

}
