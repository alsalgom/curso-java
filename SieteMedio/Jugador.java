/**
 * 
 */
package sieteMedio;

import java.util.Scanner;

/**
 * @author Iss
 *
 */
import sieteMedio.Carta;
public class Jugador {

	/**
	 * 
	 */
	
	private Carta[] mano= null;
	private String nombre = new String (" ");
	private float puntucacion;
	private final int MAXMANO = 10;
	private int cartasMano = 0;
	
	
	public Jugador() {
		// TODO Auto-generated constructor stub
		setNombre("Jugador Nuevo");
		setPuntuacion(0);
		setMano(new Carta[10]);
	}
	
	public Jugador(String nombre,int punt ){
		
		setNombre(nombre);
		setPuntuacion(punt);
		setMano(new Carta[10]);
	};
	
	public Carta[] getMano(){
		return this.mano;
	};
	
	public void imprimirMano(){
		//imprime las cartas de la mano del jugador
		String s = new String (" ");
		if(getMano().length==0)
			s = new String("VACIO");
		else
			for(Carta c:getMano())
				if (c!=null)
					System.out.println(new String(c.toString()));
	};
	
	public String getNombre(){
		return this.nombre;
	};
	
	public float getPuntuacion(){
		return this.puntucacion;
	};
	
	public void setMano(Carta[] mano){
		this.mano = mano;
	};
	
	public void setMano(Carta carta,int pos){
		//inserta una carta en una posicion
		this.mano[pos] = carta;
	};
	
	public int robarCarta(Baraja pila){
		//roba una carta de la pila y la introduce en la mano
		Carta c = pila.getCarta();
		if (c != null){
			mano[this.cartasMano] = c;
			cartasMano +=1;
			pila.eliminarCarta(c);
			sumarPuntuacion(c.getValor());
			System.out.println(c);
			if(getPuntuacion()==7.5){
				System.out.println(new String("Plantate!"));
			}
			if(getPuntuacion()>7.5){
				return 1;
			}
		}else{
			System.out.println(new String("no quedan cartas"));
		}
		return 0;
		
	};
	
	public int plantarse(){
		System.out.println(new String("Puntuacion final: "+getPuntuacion()));
		
		if(getPuntuacion()==7.5){
			System.out.println(new String("Has ganado!"));
		}else if(getPuntuacion()<7.5){
			System.out.println(new String("Casi ganas!!"));
		}
		return juegoNuevo();
	};
	
	public int juegoNuevo(){
		Scanner en = new Scanner(System.in);
		String r = null;
		System.out.println(new String("Volver a jugar??[S/N]"));
		r = new String(en.nextLine());
		switch(r){
			case "S" :
				return 0;
			case "s":
				return 0;
			case "N":
				return 1;
			case "n":
				return 1;
			default:
				return 1;
		}
	};
	
	public void setPuntuacion(int puntuacion){
		//establece la puntuacion del parametro como la nueva puntuacion
		this.puntucacion = new Integer(puntuacion);
	};
	
	public void sumarPuntuacion(double d){
		//suma 'puntos' a la puntuacion del jugador
		this.puntucacion += d;
	};
	
	public void setNombre(String nombre){
		this.nombre = new String(nombre);
	};
	
	public String toString(){
		return new String("Jugador: "+this.getNombre()+", Puntuacion: "+this.getPuntuacion());
	};
	
}
