package sieteMedio;

import sieteMedio.Jugador;

import java.util.Scanner;

import sieteMedio.Baraja;

public class app {
	
	public static Jugador uno = null;
	public static Baraja pila = null;
	public static Scanner entrada = new Scanner(System.in);
	public static int opcion = 0;
	public static int GAMEOVER = 0;
	public static int FIN = 0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//siete y medio metodo principal
		System.out.println(new String("JUEGO DEL SIETE Y MEDIO"));
		System.out.println(new String("Elije un nombre"));
		//se pide el nombre y se crea un nuevo jugador
		uno = new Jugador(entrada.next(),0);
		//se crea una nueva pila con las cartas
		pila = new Baraja();
		
		dormir(1000);
		//se muestra el jugador con sus datos
		System.out.println(new String("\nNUEVO JUGADOR CREADO"));
		System.out.println(uno.toString());
		
		dormir(1000);
		//se muestra la baraja
		System.out.println(new String("\nNUEVA BARAJA CREADA"));
		System.out.println(pila.toString());
		
		//se mezcla de nuevo la baraja
		System.out.println(new String("MEZCLANDO..."));
		pila.mezcla();
		dormir(2000);
		//empieza el juego
		System.out.println(new String("\nBARAJA MEZCLADA"));
		System.out.println(new String("\nEMPIEZA LA PARTIDA"));
		System.out.println(new String("\nBienvenido "+uno.getNombre()));
		
		do{
			//bucle principal del videojuego
			System.out.println(new String("\nQue quieres hacer:"));
			System.out.println(new String("1>Robar carta "));
			System.out.println(new String("2>Mostrar tus cartas"));
			System.out.println(new String("3>Mostrar puntuacion"));
			System.out.println(new String("4>Plantarte"));
			System.out.println(new String("5>Salir"));
			try{
				opcion = entrada.nextInt();
			}catch(Exception e){
				System.out.println("opcion incorrecta");
			}
			
			switch(opcion){
				case 1:
					//robar una carta
					GAMEOVER = uno.robarCarta(pila);
					break;
				case 2:
					//imprimir las cartas de la mano
					uno.imprimirMano();
					break;
				case 3:
					//imprimir la puntuacion
					System.out.println("Jugador: "+uno.getNombre()+", Puntuacion: "+uno.getPuntuacion());
					break;
				case 4:
					//plantarse
					FIN = uno.plantarse();
					reset();
					break;
				case 5:
					//salir del programa
					FIN = 1;
					System.out.println(new String("Hasta otra ;)"));
					break;
				default:
					//resto
					FIN = 1;
					System.out.println(new String("Salida del programa"));
					break;
			};
			if (GAMEOVER==1){
				System.out.println(new String("Has Perdido. Tu puntuacion es de "+uno.getPuntuacion()));
				FIN = uno.juegoNuevo();
				GAMEOVER=0;
				reset();
			}
			
		}while( FIN == 0 && GAMEOVER == 0);
		
	};
	
	public static void reset(){
		uno = new Jugador(uno.getNombre(),0);
		pila = new Baraja();
		pila.mezcla();
	};
	
	public static void dormir(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};

}
