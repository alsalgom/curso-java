/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerobd;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Iss
 */
public class Cajero {

    private String cuenta;
    private double saldo;
    private String cliente;
    private double cantidad;
    private ConexionDB con;
    private PreparedStatement pst;
    private VentanaAct v;
    private String mensaje;

    public Cajero() {
        this.cuenta = "";
        this.saldo = 0.0;
        this.cantidad = 0.0;
        this.cliente = "";
        this.con = new ConexionDB("Cajero");
        this.pst = null;
        this.v = new VentanaAct(this);
        this.v.setVisible(true);
        this.mensaje = "";
    }

    public ConexionDB getCon() {
        return con;
    }

    public void setCon(ConexionDB con) {
        this.con = con;
    }

    public PreparedStatement getPst() {
        return pst;
    }

    public void setPst(PreparedStatement pst) {
        this.pst = pst;
    }

    public VentanaAct getV() {
        return v;
    }

    public void setV(VentanaAct v) {
        this.v = v;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public boolean ingreso(double cantidad) {
        String consulta = "UPDATE cajero.cuentas "
                + "SET cuentas.saldo=((select cuentas.saldo where cuentas.codigo = ?)+?)"
                + " where cuentas.codigo = ?";
        try {
            if (comprobarCliente(this.cliente)) {
                this.pst = this.con.getCon().prepareStatement(consulta);
                this.mensaje = this.mensaje + "\n Ingreso en cuenta: " + this.cliente + " de " + this.cantidad + "";
                return con.ejecutarConsultaPST(this.pst, this.cliente, this.cantidad);

            }
        } catch (SQLException ex) {
            this.mensaje += "\nError en la operacion Ingeso dinero";
            return false;
        }
        return false;

    }

    public boolean sacarDinero(double cantidad) {

        String consulta = "UPDATE cajero.cuentas "
                + "SET cuentas.saldo=((select cuentas.saldo where cuentas.codigo = ?)-?)"
                + " where cuentas.codigo = ?";

        try {
            if (comprobarCliente(this.cliente) && comprobarSaldo()) {
                this.pst = this.con.getCon().prepareStatement(consulta);
                this.mensaje = this.mensaje + "\n Reintegro en cuenta: " + this.cliente + " de " + this.cantidad + "";
                return con.ejecutarConsultaPST(this.pst, this.cliente, this.cantidad);
            }
        } catch (SQLException ex) {
            this.mensaje += "\nError en la operacion Sacar dinero";
            return false;
        }

        return false;

    }

    public boolean transferencia(double cantidad) {

        String consulta1 = "UPDATE cuentas "
                + "SET saldo=(select cuenta.saldo where cuentas.codigo = ?)-? "
                + "where cuentas.codigo= ?";
        String consulta2 = "UPDATE cuentas "
                + "SET saldo=(select cuenta.saldo where cuentas.codigo = ?)+? "
                + "where cuentas.codigo= ?";
        try {

            if (comprobarCliente(this.cliente) && comprobarCliente(this.cuenta) && comprobarSaldo()) {
                this.pst = this.con.getCon().prepareStatement(consulta1);
                con.ejecutarConsultaPST(this.pst, this.cliente, this.cantidad);
                this.pst = this.con.getCon().prepareStatement(consulta2);
                con.ejecutarConsultaPST(this.pst, this.cuenta, this.cantidad);
                this.mensaje = "\nTransferencia correcta de cliente: " + this.cliente + "\n al cliente: " + this.cuenta + "\n por importe de : " + this.cantidad;
                return true;
            }

        } catch (SQLException ex) {
            this.mensaje += "\nError en la operacion transferencia";
            return false;
        }
        return false;
    }

    public boolean comprobarSaldo() {
        String sql = "Select cuentas.saldo from cajero.cuentas where cuentas.codigo = " + this.cliente + ";";
        this.con.ejecutarConsultaST(sql);

        try {
            double s = 0;
            if (con.getResultado().next()) {
                s = (double) con.getResultado().getInt("Saldo");
            }

            if (s >= this.cantidad) {
                return true;
            } else {
                this.mensaje += "\nSaldo insuficiente";
            }
            this.con.getResultado().close();
        } catch (SQLException ex) {
            this.mensaje += "\nError en la operacion,No se comprobo el saldo";
            return false;
        }
        return false;
    }

    public boolean comprobarCliente(String cl) {
        String sql = "Select cuentas.codigo from cajero.cuentas where cuentas.codigo = " + cl + ";";
        this.con.ejecutarConsultaST(sql);

        try {

            if (con.getResultado().next() == false) {
                this.mensaje += "\nEl Cliente No Existe " + cl;
                return false;
            } else {
                this.mensaje += "\nCliente encontrado";
                return true;
            }

        } catch (SQLException ex) {
            this.mensaje += "\nError en la operacion,No se comprobo el cliente";
            return false;
        }

    }

}
