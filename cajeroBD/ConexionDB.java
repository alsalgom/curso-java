/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajerobd;

import java.sql.*;

/**
 *
 * @author Iss
 */
public class ConexionDB {

    private Connection con;
    private String bdurl;
    private String bdusr;
    private String bdpass;
    private Statement st;
    private PreparedStatement pst;
    private ResultSet resultado;
    private String consulta;

    public ConexionDB(String bd) {
        this.bdusr = "root";
        this.bdpass = "cursojava";
        this.bdurl = "jdbc:mysql://localhost:3306/" + bd;
        this.con = null;
        this.st = null;
        this.pst = null;
        this.resultado = null;
        this.consulta = "";
        abrirConexion();

    }

    public boolean abrirConexion() {
        try {
            this.con = DriverManager.getConnection(bdurl, bdusr, bdpass);
            this.st = con.prepareStatement("");
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String getBdurl() {
        return bdurl;
    }

    public void setBdurl(String bdurl) {
        this.bdurl = bdurl;
    }

    public String getBdusr() {
        return bdusr;
    }

    public void setBdusr(String bdusr) {
        this.bdusr = bdusr;
    }

    public String getBdpass() {
        return bdpass;
    }

    public void setBdpass(String bdpass) {
        this.bdpass = bdpass;
    }

    public Statement getSt() {
        return st;
    }

    public void setSt(Statement st) {
        this.st = st;
    }

    public PreparedStatement getPst() {
        return pst;
    }

    public void setPst(PreparedStatement pst) {
        this.pst = pst;
    }

    public ResultSet getResultado() {
        return resultado;
    }

    public void setResultado(ResultSet resultado) {
        this.resultado = resultado;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public boolean ejecutarConsultaPST(PreparedStatement pst, String cuenta, double cantidad) {
        this.pst = pst;
        if (con != null) {
            try {

                this.pst.setString(0, cuenta);
                this.pst.setDouble(1, cantidad);
                this.pst.setString(2, cuenta);
                boolean res = pst.execute();

                return res;

            } catch (SQLException ex) {
                return false;
            }

        }
        return false;
    }

    public boolean ejecutarConsultaST(String sql) {
        this.setConsulta(sql);
        if (con != null) {
            try {
                st = con.createStatement();
                resultado = st.executeQuery(this.consulta);

                return true;

            } catch (SQLException ex) {
                return false;
            }

        }
        return false;
    }

    public void cerrarCon() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (resultado != null) {
            try {
                resultado.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
