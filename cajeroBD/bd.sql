create database cajero;
use cajero;
CREATE TABLE cuentas (
	codigo varchar(50) unique,
    cliente VARCHAR(50),
    email varchar(50),
    saldo double,
    primary key(codigo)
);

INSERT INTO cuentas VALUES('1234567', 'Pepito', 'pepito@pepitomail.com', 0.0);
INSERT INTO cuentas VALUES('2345678', 'Menganito', 'Menganito@Menganitomail.com', 20.0);
INSERT INTO cuentas VALUES('3456789', 'Jaimito', 'Jaimito@Jaimitomail.com', 2045.34);