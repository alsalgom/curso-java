package swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Font;

public class swing extends JFrame {

    private JTextField textField;
    private String op1, op2;
    private int num1, num2;
    private int opcion;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    swing frame = new swing();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public swing() {
        this.op1 = "";
        this.op2 = "";
        this.num1 = 0;
        this.num2 = 0;
        this.opcion = 10;

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 351, 365);
        getContentPane().setLayout(null);

        JButton btn0 = new JButton("0");
        btn0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(0);

            }
        });
        btn0.setBounds(102, 259, 57, 32);
        getContentPane().add(btn0);

        JButton button = new JButton(".");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });
        button.setBounds(37, 259, 57, 32);
        getContentPane().add(button);

        JButton button_1 = new JButton("=");
        button_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calcula(swing.this.op1, swing.this.op2, swing.this.opcion);
                opcion = 10;
                swing.this.op1 = "";
                swing.this.op2 = "";
            }
        });
        button_1.setBounds(169, 259, 57, 32);
        getContentPane().add(button_1);

        JButton btn1 = new JButton("1");
        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(1);
            }
        });
        btn1.setBounds(37, 216, 57, 32);
        getContentPane().add(btn1);

        JButton btn2 = new JButton("2");
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(2);
            }
        });
        btn2.setBounds(101, 216, 57, 32);
        getContentPane().add(btn2);

        JButton btn3 = new JButton("3");
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(3);
            }
        });
        btn3.setBounds(168, 216, 57, 32);
        getContentPane().add(btn3);

        JButton btn4 = new JButton("4");
        btn4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(4);
            }
        });
        btn4.setBounds(37, 171, 57, 32);
        getContentPane().add(btn4);

        JButton btn5 = new JButton("5");
        btn5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(5);
            }
        });
        btn5.setBounds(101, 171, 57, 32);
        getContentPane().add(btn5);

        JButton btn6 = new JButton("6");
        btn6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(6);
            }
        });
        btn6.setBounds(168, 171, 57, 32);
        getContentPane().add(btn6);

        JButton btn7 = new JButton("7");
        btn7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(7);
            }
        });
        btn7.setBounds(37, 128, 57, 32);
        getContentPane().add(btn7);

        JButton btn8 = new JButton("8");
        btn8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(8);
            }
        });
        btn8.setBounds(102, 128, 57, 32);
        getContentPane().add(btn8);

        JButton btn9 = new JButton("9");
        btn9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                a�adirNum(9);

            }
        });
        btn9.setBounds(169, 128, 57, 32);
        getContentPane().add(btn9);

        JButton btnNewButton_1 = new JButton("+");
        btnNewButton_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                swing.this.opcion = 1;
            }
        });
        btnNewButton_1.setBounds(236, 259, 57, 32);
        getContentPane().add(btnNewButton_1);

        JButton btnNewButton_2 = new JButton("-");
        btnNewButton_2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                swing.this.opcion = 2;
            }
        });
        btnNewButton_2.setBounds(236, 216, 57, 32);
        getContentPane().add(btnNewButton_2);

        JButton btnNewButton_3 = new JButton("*");
        btnNewButton_3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                swing.this.opcion = 3;
            }
        });
        btnNewButton_3.setBounds(235, 171, 57, 32);
        getContentPane().add(btnNewButton_3);

        JButton btnNewButton_4 = new JButton("/");
        btnNewButton_4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                swing.this.opcion = 4;
            }
        });
        btnNewButton_4.setBounds(236, 128, 57, 32);
        getContentPane().add(btnNewButton_4);

        JButton btnNewButton_5 = new JButton("C");
        btnNewButton_5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (opcion == 10) {
                    swing.this.op1 = "";

                } else {
                    swing.this.op2 = "";
                }
                swing.this.textField.setText("0");
                swing.this.num1 = 0;
                swing.this.num2 = 0;
            }
        });
        btnNewButton_5.setBounds(236, 85, 57, 32);
        getContentPane().add(btnNewButton_5);

        textField = new JTextField();
        textField.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
        textField.setBackground(Color.WHITE);
        textField.setForeground(Color.DARK_GRAY);
        textField.setEditable(false);
        textField.setBounds(47, 11, 226, 56);
        getContentPane().add(textField);
        textField.setColumns(10);
    }

    public void a�adirNum(int num) {
        if (swing.this.opcion == 10) {
            swing.this.op1 += num + "";
            System.out.println(swing.this.op1);
            swing.this.textField.setText(op1);
        } else {
            swing.this.op2 += num + "";
            swing.this.textField.setText(op2);
        }
    }

    public void calcula(String opt1, String opt2, int op) {
        swing.this.num1 = Integer.parseInt(opt1);
        swing.this.num2 = Integer.parseInt(opt2);
        StringBuilder res = new StringBuilder();
        int r1 = 0;
        res.append(opt1);

        switch (op) {
            case 1:
                //suma
                res.append(" + ");
                r1 = swing.this.num1 + swing.this.num2;

                break;
            case 2:
                //resta
                res.append(" - ");
                r1 = swing.this.num1 - swing.this.num2;
                break;
            case 3:
                //multiplicacion
                res.append(" * ");
                r1 = swing.this.num1 * swing.this.num2;
                break;
            case 4:
                //division
                res.append(" / ");
                r1 = swing.this.num1 / swing.this.num2;
                break;
        }
        res.append(opt2).append(" = ").append(r1);
        swing.this.textField.setText(res.toString());
    }
;
}
