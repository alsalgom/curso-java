/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hora;

/**
 *
 * @author Iss
 */
public class Hora {

    private int horas;
    private int minutos;
    private int segundos;

    public Hora() {
        this.horas = 00;
        this.minutos = 00;
        this.segundos = 00;
    }

    public Hora(int horas, int minutos, int segundos) {

        if (valida(horas, minutos, segundos)) {
            this.horas = horas;
            this.minutos = minutos;
            this.segundos = segundos;
        } else {

            ajusta(horas, minutos, segundos);

        }

    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    public void leer(int h, int m, int s) {
        if (valida(h, m, s)) {
            this.setHoras(h);
            this.setMinutos(m);
            this.setSegundos(s);

        } else {
            ajusta(h, m, s);
        }
    }

    private boolean valida(int hora, int minuto, int segundo) {
        return ((hora > 0 && hora < 25) && (minuto < 60 && minuto > 0) && (segundo < 60 && segundo > 0));
    }

    private void ajusta(int h, int m, int s) {

        while (s > 60) {
            m = m + 1;
            s = s - 60;
        }
        while (m > 60) {
            h = h + 1;
            m = m - 60;
        }
        int hora[] = {h, m, s};
        if (h > 24) {
            throw new IllegalArgumentException("hora erronea");
        }
        this.setHoras(hora[0]);
        this.setMinutos(hora[1]);
        this.setSegundos(hora[2]);
    }

    public void print() {
        this.toString();
    }

    public int aSegundos() {
        int h1 = this.getHoras(), m1 = this.getMinutos(), s1 = this.getSegundos();
        while (h1 > 0) {
            m1 = m1 + 60;
            h1 = h1 - 1;
        }
        while (m1 > 0) {
            s1 = s1 + 60;
            m1 = m1 - 1;

        }
        return s1;
    }

    public void deSegundos(int s) {
        int h1 = 0, m1 = 0, s1 = s;
        ajusta(h1, m1, s1);
    }

    public String segundosDesde(Hora h) {
        int h1 = this.getHoras(), h2 = h.getHoras(), m1 = this.getMinutos(), m2 = h.getMinutos(), s1 = this.getSegundos(), s2 = h.getSegundos();
        int segres = 0;
        if (h1 > h2) {
            while (h1 < 24) {
                h1 += 1;
                segres += 3600;
            }
            h1 = 0;
        }
        while (h1 < h2) {
            h1 += 1;
            segres += 3600;

        }
        while (m1 < m2) {
            m1 += 1;
            segres += 60;
        }
        while (s1 < s2) {
            s1 += 1;
            segres += 1;
        }
        return "han pasado: " + segres + " segundos";
    }

    public void siguiente() {
        this.setSegundos(this.getSegundos() + 1);
        ajusta(this.getHoras(), this.getMinutos(), this.getSegundos());
    }

    public void anterior() {
        this.setSegundos(this.getSegundos() - 1);
        if (this.getSegundos() < 0) {
            this.setMinutos(this.getMinutos() - 1);
            this.setSegundos(0);
        }
    }

    public Hora copia() {
        return new Hora(this.getHoras(), this.getMinutos(), this.getSegundos());
    }

    public boolean igualQue(Hora h) {
        return (this.getHoras() == h.getHoras() && this.getMinutos() == h.getMinutos() && this.getSegundos() ==h.getSegundos());
    }

    public boolean menosQue(Hora h) {
        return(this.getHoras()<h.getHoras()||this.getMinutos()<h.getMinutos()||this.getSegundos()<h.getSegundos());
    }

    public boolean mayorQue(Hora h) {
        return(this.getHoras()>h.getHoras()||this.getMinutos()>h.getMinutos()||this.getSegundos()>h.getSegundos());
    }

    @Override
    public String toString() {
        String h = Integer.toString(this.getHoras()), m = Integer.toString(this.getMinutos()), s = Integer.toString(this.getSegundos());
        if (this.getHoras() < 10) {
            h = "0" + Integer.toString(this.getHoras());
        }
        if (this.getMinutos() < 10) {
            m = "0" + Integer.toString(this.getMinutos());
        }
        if (this.getSegundos() < 10) {
            s = "0" + Integer.toString(this.getSegundos());
        }
        return "[" + h + ":" + m + ":" + s + "]";
    }

}
