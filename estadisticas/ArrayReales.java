/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estadisticas;

/**
 *
 * @author Iss
 */
public class ArrayReales extends Estadisticas {

    double[] arrayReales;

    public ArrayReales() {
    }

    public ArrayReales(double[] arrayReales) {
        this.arrayReales = arrayReales;
    }

    public double[] getArrayReales() {
        return arrayReales;
    }

    public void setArrayReales(double[] arrayReales) {
        this.arrayReales = arrayReales;
    }

}
