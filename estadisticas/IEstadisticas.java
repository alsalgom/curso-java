/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estadisticas;

/**
 *
 * @author Iss
 */
public interface IEstadisticas {

    public int maximo(double[] d);

    public int minimo(double[] d);

    public int sumatorio(double[] d);
}
