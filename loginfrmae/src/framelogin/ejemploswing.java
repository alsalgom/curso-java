/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package framelogin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Iss
 */
public class ejemploswing extends JFrame{
    private int ancho,alto;
    private JButton btn;

    public ejemploswing() {
        setTitle("ejemplo SWING");
        ancho = 600;
        alto= 400;
        setBounds(150,100,ancho,alto);
        setVisible(true);
        setLayout(null);
        this.addContorls();
    }
    private void addContorls(){
        this.btn = new JButton("click");
        this.btn.setBounds(170,120,150,50);
        this.btn.setVisible(true);
        this.btn.addActionListener(new ActionListener(){
            

            @Override
            public void actionPerformed(ActionEvent e) {
               System.out.println("Click");
            }
        });
        this.getContentPane().add(btn);
    }
    
}
