/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;

import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Iss
 */
@WebServlet(
        name = "login",
        urlPatterns = {"/login"},
        initParams = @WebInitParam(
                name = "bdUrl",
                value = "jdbc:mysql://localhost:3306/"
        )
)
public class login extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 8051228693025523534L;
    /**
     *
     */

    Set<Usuario> users;
    DbConexion con;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.users = new HashSet<Usuario>();
        response.setContentType("text/html;charset=UTF-8");
        
        ServletContext context = getServletContext();
        String bd = getInitParameter("bdUrl")+"userslog";
        this.con = new DbConexion(bd);
        con.abrirConexion();

//        Usuario a = new Usuario("admin", "admin");
//        Usuario b = new Usuario("root", "root");
//        Usuario c = new Usuario("user", "user");
//        Usuario d = new Usuario("prueba", "prueba");
//        Usuario e = new Usuario("ejemlo", "ejemplo");
//        users.add(a);
//        users.add(b);
//        users.add(c);
//        users.add(d);
//        users.add(e);
        try (PrintWriter out = response.getWriter()) {
            
            String nombre = request.getParameter("User");
            String contrasena = request.getParameter("Passwd");
            Usuario u = new Usuario(nombre, contrasena);
            con.ejecSql("Select * from users");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet login</title>");
            out.println("<meta http-equiv=\"refresh\" content=\"5;URL='./login.html'\" /> ");
            out.println("</head>");
            out.println("<body>");
            if (comprobarUser(u)) {
                //getRequestDispatcher()
                response.sendRedirect("./logeado.html");
            } else {
                out.append("<h3 style='color:red;'>Error de login</h3>");
            }
            out.println("</body>");
            out.println("</html>");
        }
        con.cierre();
    }

    public boolean rellenarUsers(PrintWriter out) {
        try {
            ResultSet res = login.this.con.getResultado();

            if (res != null) {
                while (res.next()) {
                    String usu = res.getString("usu");
                    String passwd = res.getString("passwd");

                    Usuario u = new Usuario(usu, passwd);
                    this.users.add(u);
                    return true;
                }
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
            return false;
        }
        return false;
    }

    public boolean comprobarUser(Usuario u) {

        if (!this.users.isEmpty()) {
            for (Iterator<Usuario> it = this.users.iterator(); it.hasNext();) {
                Usuario usu = it.next();
                if (u.equals(usu)) {
                    return true;
                }
            }
        }

        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
