<%-- 
    Document   : index
    Created on : 17-oct-2016, 8:45:23
    Author     : Iss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>Servicio de mensajeria</title>
        <link rel="stylesheet" type="text/css" href="theme.css"/>
    </head>
    <body>
        <h2>Login</h2>
        <form action="./login" method="post">

            <table >
                <thead>
                    <tr>
                        <th colspan="2">Datos Acceso</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Usuario</td>
                        <td><input type="text" name="uname" value="" /></td>
                    </tr>
                    <tr>
                        <td>Contraseña</td>
                        <td><input type="password" name="pass" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Login" /></td>
                        <td><input type="reset" value="Reset" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"> <a href="./Registro">Registro Usuario</a></td>
                    </tr>
                </tbody>
            </table>

        </form>
    </body>
</html>
