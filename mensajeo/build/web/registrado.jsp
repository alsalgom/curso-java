<%-- 
    Document   : registrado.jsp
    Created on : 18-oct-2016, 10:37:36
    Author     : Iss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>mensajero: logeado</title>
        <link rel="stylesheet" type="text/css" href="theme.css"/>
    </head>
    <body>
        <%
            String user = null;
            if (session.getAttribute("user") == null) {
                response.sendRedirect("index.jsp");
            } else {
                user = (String) session.getAttribute("user");
            }
            String userName = null;
            String sessionID = null;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("user")) {
                        userName = cookie.getValue();
                    }
                    if (cookie.getName().equals("JSESSIONID")) {
                        sessionID = cookie.getValue();
                    }
                }
            }
        %>
        
        <h2>Hola <%=user%>, sesion ID=<%=sessionID%></h2>
        <h3>Opciones:</h3>
        <div>
            <div style="">
                <form method="post" action="./envio_correo">
                    <table >
                        <thead>
                            <tr>
                                <th colspan="2">Enviar un mensaje</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Destinatario:</td>
                                <td><input type="text" name="destin" value="" /></td>
                            </tr>
                            <tr>
                                <td>Asunto:</td>
                                <td><input type="text" name="asunto" value="" /></td>
                            </tr>
                            <tr>
                                <td>Mensaje: </td>
                                <td><textarea rows="6" cols="50" name="mensaje"></textarea></td>
                            </tr>
                            <tr>
                                <td><input type="submit" value="Enviar" /></td>
                                <td><input type="reset" value="Reset" /></td>
                            </tr>
                        </tbody>
                    </table>
                </form>  
            </div>
            <div>
                <form method="post" action="./contactos">
                    <table >
                        <thead>
                            <tr>
                                <th>Mostrar contactos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="submit" value="Mostrar Contactos" /></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
                <form method="post" action="./recibidos">
                    <table >
                        <thead>
                            <tr>
                                <th>Mensajes Recibidos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="submit" value="Mostrar Mensajes" /></td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
