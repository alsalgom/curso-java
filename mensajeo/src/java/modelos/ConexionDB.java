package modelos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Iss
 */
public class ConexionDB {

    private Connection con;
    private String bdurl;
    private String bdusr;
    private String bdpass;
    private Statement st;
    private ResultSet resultado;
    private String consulta;

    public ConexionDB(String bdurl) {
        this.bdusr = "root";
        this.bdpass = "cursojava";
        this.bdurl = bdurl;
        this.con = null;
        this.st = null;
        this.resultado = null;
        this.consulta = "";
    }

    public boolean abrirConexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.con = DriverManager.getConnection(bdurl, bdusr, bdpass);
            return true;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public Connection obtenerConexion() {
        return this.getCon();
    }

    public void conectar() {
        abrirConexion();
    }

    public boolean ejecSql(String sql) {
        this.setConsulta(sql);
        if (con != null) {
            try {
                st = con.createStatement();
                resultado = st.executeQuery(this.consulta);

                return true;

            } catch (SQLException ex) {
                return false;
            }

        }
        return false;
    }

    public void cierre() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (resultado != null) {
            try {
                resultado.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String getBdurl() {
        return bdurl;
    }

    public void setBdurl(String bdurl) {
        this.bdurl = bdurl;
    }

    public String getBdusr() {
        return bdusr;
    }

    public void setBdusr(String bdusr) {
        this.bdusr = bdusr;
    }

    public String getBdpass() {
        return bdpass;
    }

    public void setBdpass(String bdpass) {
        this.bdpass = bdpass;
    }

    public Statement getSt() {
        return st;
    }

    public void setSt(Statement st) {
        this.st = st;
    }

    public ResultSet getResultado() {
        return resultado;
    }

    public void setResultado(ResultSet resultado) {
        this.resultado = resultado;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

}
