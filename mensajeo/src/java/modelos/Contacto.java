package modelos;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Iss
 */
public class Contacto {

    private int idcontacto;
    private int usuario;
    private String nombre;
    private String apellido;
    private String correo;
    private String direccion;

    public Contacto() {
        this.idcontacto = 0;
        this.usuario = 0;
        this.nombre = "";
        this.apellido = "";
        this.correo = "";
        this.direccion = "";
    }
    public Contacto(int usuario, String nombre, String apellido, String correo, String direccion) {
        this.idcontacto = 0;
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.direccion = direccion;
    }
    public Contacto(int idcontacto, int usuario, String nombre, String apellido, String correo, String direccion) {
        this.idcontacto = idcontacto;
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.direccion = direccion;
    }

    public int getIdcontacto() {
        return idcontacto;
    }

    public void setIdcontacto(int idcontacto) {
        this.idcontacto = idcontacto;
    }

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    

}
