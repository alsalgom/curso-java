package servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelos.ConexionDB;

/**
 *
 * @author Iss
 */
@WebServlet(
        name = "envio_correo",
        urlPatterns = {"/envio_correo"},
        initParams = @WebInitParam(
                name = "bdUrl",
                value = "jdbc:mysql://localhost:3306/"
        )
)
public class envio_correo extends HttpServlet {

    private HttpSession session;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private ConexionDB con;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* Conexion bd */
            ServletContext context = getServletContext();
            String bd = getInitParameter("bdUrl") + "correo";
            this.con = new ConexionDB(bd);
            con.abrirConexion();

            //revisar session y usuario 
            session = request.getSession();
            String user = null;
            if (session.getAttribute("user") == null) {
                response.sendRedirect("index.jsp");
            } else {
                user = (String) session.getAttribute("user");
            }
            String userName = null;
            String sessionID = null;
            int id = 0;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("user")) {
                        userName = cookie.getValue();
                    }
                    if (cookie.getName().equals("id")) {
                        id = Integer.parseInt(cookie.getValue());
                    }
                    if (cookie.getName().equals("JSESSIONID")) {
                        sessionID = cookie.getValue();
                    }
                }
            }

            //recibir datos del mensaje
            String destin = request.getParameter("destin");
            String asunto = request.getParameter("destin");
            String mensaje = request.getParameter("mensaje");

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet envio_correo</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("");
            out.println("<h1>Envio mensaje</h1>");
            boolean res = false;
            res = con.ejecSql("insert into correo.mensajes (remitente,asunto,mensajes,enviado) \n"
                    + "values('" + id + "','" + asunto + "','" + mensaje + "','0');");
            if (res) {
                out.println("<p>mensaje registrado</p>");
                res = con.ejecSql("insert into correo.mensajes_has_contactos value("
                        + "(select mensajes.idMensajes from correo.mensajes where remitente ='" + id + "' and asunto ='" + asunto + "' and mensaje ='" + mensaje + "'),"
                        + "(select contactos.idcontactos from correo.contactos where contactos.nombre ='" + destin + "'or contactos.direccion ='" + destin + "');");
                if (res) {
                    res= con.ejecSql("update correo.mensajes set enviado = '1' where id = '"+id+"' and asunto = '"+asunto+"' and mensaje = '"+mensaje+"';");
                    out.println("<p>Mensaje enviado correctamente</p>");
                }
            }else{
                out.println("<p>Mensaje no enviado</p>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
