/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;

import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelos.Usuario;
import modelos.ConexionDB;

/**
 *
 * @author Iss
 */
@WebServlet(
        name = "login",
        urlPatterns = {"/login"},
        initParams = @WebInitParam(
                name = "bdUrl",
                value = "jdbc:mysql://localhost:3306/"
        )
)
public class login extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 8051228693025523534L;
    /**
     *
     */

    private Set<Usuario> users;
    private ConexionDB con;
    private int id;
    

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.users = new HashSet<Usuario>();
        response.setContentType("text/html;charset=UTF-8");

        ServletContext context = getServletContext();
        String bd = getInitParameter("bdUrl") + "correo";
        this.con = new ConexionDB(bd);

        con.abrirConexion();

        try (PrintWriter out = response.getWriter()) {

            String nombre = request.getParameter("uname");
            String contrasena = request.getParameter("pass");

            Usuario u = new Usuario(nombre, contrasena);

            con.ejecSql("Select * from correo.usuarios where usuarios.user = '" + u.getUser() + "' and usuarios.passwd = '" + u.getPasswd() + "';");
            rellenarUsers();

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet login</title>");
            out.println("<meta http-equiv=\"refresh\" content=\"5;URL='./index.jsp'\" /> ");
            out.println("</head>");
            out.println("<body>");

            if (comprobarUser(u)) {
                //getRequestDispatcher()
                HttpSession session = request.getSession();
			session.setAttribute("user", nombre);
                        session.setAttribute("id",id);
			//setting session to expiry in 30 mins
			session.setMaxInactiveInterval(30*60);
			Cookie userName = new Cookie("user", nombre);
			response.addCookie(userName);
			
                System.out.println("Login Correcto");
                con.cierre();
                response.sendRedirect("./registrado.jsp");

            } else {
                con.cierre();
                System.out.println("Login Incorrecto");
                out.append("<h3 style='color:red;'>Error de login</h3>");
            }

            out.println("</body>");
            out.println("</html>");
        }
        con.cierre();
    }

    public boolean rellenarUsers() {
        try {
            ResultSet res = login.this.con.getResultado();

            if (res != null) {

                while (res.next()) {
                    this.id = res.getInt("idusuarios");
                    String usu = res.getString("user");
                    String passwd = res.getString("passwd");

                    Usuario u = new Usuario(usu, passwd);
                    u.setIduser(id);
                    System.out.println("usuario leido: " + u.toString());
                    this.users.add(u);
                    return true;
                }
                res.close();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return false;
    }

    public boolean comprobarUser(Usuario u) {

        if (!this.users.isEmpty()) {
            for (Usuario usu : this.users) {
                System.out.println(usu.toString());
                if (u.equals(usu)) {
                    return true;
                }
            }
        }

        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
