/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Iss
 */
@WebServlet(name = "CuantosDias", urlPatterns = {"/CuantosDias"})
public class CuantosDias extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int dias = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String fecha2 = request.getParameter("fecha");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CuantosDias</title>");
            out.println("</head>");
            out.println("<body>");

            out.println("<h2>Contar los dias desde la fecha de tu cumpleaños</h2>");

            if (comprobarfecha(fecha2)) {
                contardias(fecha2);
                out.println("<p>Fecha introduciada: " + fecha2 + "</p>");
                out.println("<p>Han pasado " + this.dias + " dias</p>");
            } else {
                out.println("<p style='color:red;'>No has introducido una fecha valida</p>");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    public boolean comprobarfecha(String fecha) {
        Pattern p = Pattern.compile("../..");
        Matcher m = p.matcher(fecha);
        boolean b = m.matches();

        if (fecha.equals("") || fecha == null || !b) {
            return false;
        } else {
            String[] f = fecha.split("/");
            int dia = Integer.parseInt(f[0]), mes = Integer.parseInt(f[1]);
            if ((dia < 32 && dia > 0) && (mes < 13 && mes > 0)) {
                return true;
            }
        }
        return false;
    }

    public void contardias(String fecha) {
        String[] f = fecha.split("/");
        Date fecha1 = new Date();
        this.dias=0;
        int dia = Integer.parseInt(f[0]), mes = Integer.parseInt(f[1]), dia2 = fecha1.getDate(), mes2 = (fecha1.getMonth() + 1);
        if (mes < mes2) {

            for (int i = mes+1; i <= mes2; i++) {
                this.dias += 31;
            }
            this.dias-= (30-dia);
            this.dias+= (dia2);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
