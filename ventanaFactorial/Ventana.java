/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorialframe;

import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author Iss
 */
public final class Ventana extends Frame {

    private int HEIGHT;
    private int WIDTH;
    private String titulo;
    private int posX;
    private int posY;
    private boolean visible;
    private Button calcButton;
    private TextField txtEnt, txtSal;
    private Label labTit, labErr;

    public Ventana(String title) throws HeadlessException {
        super(title);
        this.HEIGHT = 400;
        this.WIDTH = 500;
        this.titulo = title;
        this.posX = 300;
        this.posY = 150;
        this.visible = true;
        this.setLayout(null);
        initFrame();
        addComponents();

    }

    public void initFrame() {
        super.setBounds(posX, posY, WIDTH, HEIGHT);
        super.setTitle(titulo);
        super.setVisible(visible);
        super.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }

    public void addComponents() {
        this.labTit = new Label();
        this.labErr = new Label();
        this.txtEnt = new TextField();
        this.txtSal = new TextField();

        this.txtSal.setEditable(false);
        this.labTit.setText("Factorial");
        this.calcButton = new Button("Calcula");

        this.calcButton.setBounds(300, 260, 60, 30);
        this.labTit.setBounds(100, 160, 60, 30);
        this.txtEnt.setBounds(300, 100, 60, 30);
        this.txtSal.setBounds(300, 180, 60, 30);
        this.labErr.setBounds(300, 150, 92, 30);

        this.calcButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Ventana.this.labErr.setText(" ");
                    Ventana.this.txtSal.setText(String.valueOf(calculaFactorial(Integer.valueOf(Ventana.this.txtEnt.getText()))));
                } catch (NumberFormatException ex) {
                    Ventana.this.labErr.setText("Error de entrada");
                }

            }
        });

        super.add(this.calcButton);
        super.add(this.labTit);
        super.add(this.txtEnt);
        super.add(this.txtSal);
        super.add(this.labErr);

    }

    public int calculaFactorial(int i) {
        int res = 1;
        while (i != 0) {
            res = res * i;
            i--;
        }
        return res;
    }

    ;
    public int getHEIGHT() {
        return HEIGHT;
    }

    public void setHEIGHT(int HEIGHT) {
        this.HEIGHT = HEIGHT;
    }

    public int getWIDTH() {
        return WIDTH;
    }

    public void setWIDTH(int WIDTH) {
        this.WIDTH = WIDTH;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public Button getCalcButton() {
        return calcButton;
    }

    public void setCalcButton(Button calcButton) {
        this.calcButton = calcButton;
    }

    public TextField getTxtEnt() {
        return txtEnt;
    }

    public void setTxtEnt(TextField txtEnt) {
        this.txtEnt = txtEnt;
    }

    public TextField getTxtSal() {
        return txtSal;
    }

    public void setTxtSal(TextField txtSal) {
        this.txtSal = txtSal;
    }

    public Label getLabTit() {
        return labTit;
    }

    public void setLabTit(Label labTit) {
        this.labTit = labTit;
    }

    public Label getLabErr() {
        return labErr;
    }

    public void setLabErr(Label labErr) {
        this.labErr = labErr;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

}
