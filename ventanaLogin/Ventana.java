/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package framelogin;

import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Iss
 */
public final class Ventana extends Frame {

    private int HEIGHT, WIDTH, posX, posY;
    private String titulo, usuario, conrasenya;
    private boolean visible;
    private Button loginButton;
    private TextField userField, passwdField;
    private Label userLabel, passwdLabel, errorField;
    private Map<String, String> users;

    public Ventana(String title) throws HeadlessException {
        super(title);
        this.HEIGHT = 400;
        this.WIDTH = 500;
        this.titulo = title;
        this.posX = 300;
        this.posY = 150;
        this.visible = true;
        this.setLayout(null);
        initFrame();
        addComponents();
        this.users = new HashMap<String, String>();
        crearUsuPrueba();
    }

    public void initFrame() {
        super.setBounds(posX, posY, WIDTH, HEIGHT);
        super.setTitle(titulo);
        super.setVisible(visible);
        super.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }

    public void addComponents() {

        this.errorField = new Label();
        this.errorField.setBounds(130, 290, 250, 30);
        super.add(this.errorField);

        this.userLabel = new Label("Usuario");
        this.userLabel.setBounds(100, 100, 60, 30);
        super.add(this.userLabel);

        this.userField = new TextField();
        this.userField.setBounds(300, 100, 90, 30);
        super.add(this.userField);

        this.passwdLabel = new Label("Pasword");
        this.passwdLabel.setBounds(100, 160, 60, 30);
        super.add(this.passwdLabel);

        this.passwdField = new TextField();
        this.passwdField.setBounds(300, 160, 90, 30);
        this.passwdField.setEchoChar('*');
        this.passwdField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                char c = e.getKeyChar();
                System.out.println(c);
            }
        });
        super.add(this.passwdField);

        this.loginButton = new Button("Login");
        this.loginButton.setBounds(300, 260, 60, 30);
        this.loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Ventana.this.setUsuario(Ventana.this.userField.getText());
                Ventana.this.setConrasenya(Ventana.this.passwdField.getText());
                if (Ventana.this.getUsuario().equals("")) {
                    Ventana.this.errorField.setText("Introduce el usuario");
                } else if (Ventana.this.getConrasenya().equals("")) {
                    Ventana.this.errorField.setText("introduce la contasenya");
                } else if (users.containsKey(Ventana.this.getUsuario())) {
                    if (users.get(Ventana.this.getUsuario()).equals(Ventana.this.getConrasenya())) {
                        Ventana.this.errorField.setText("Usuario correcto");
                    } else {
                        Ventana.this.errorField.setText("Usuario registrado,contrasenya incorrecta");
                    }

                } else {
                    Ventana.this.users.put(Ventana.this.getUsuario(), Ventana.this.getConrasenya());
                    Ventana.this.errorField.setText("Usuario no regristrado. Añadido");
                }
            }
        });
        super.add(this.loginButton);
    }

    public void crearUsuPrueba() {
        users.put("prueba", "prueba");
        users.put("admin", "1234");
        users.put("uno", "passuno");
        users.put("dos", "passdos");
        users.put("tres", "passtres");
        users.put("juan", "juanpasswd");
        users.put("marta", "martapasswd");
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getConrasenya() {
        return conrasenya;
    }

    public void setConrasenya(String conrasenya) {
        this.conrasenya = conrasenya;
    }

    public Map<String, String> getUsers() {
        return users;
    }

    public void setUsers(Map<String, String> users) {
        this.users = users;
    }

}
